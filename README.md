# TODO List

- bootstrap js 여러번 중첩 콜 되서 망가지는 경우 있음
https://wordpress.stackexchange.com/questions/123547/enqueue-script-only-if-it-is-not-already-enqueue




# TOI

1. Vagrant(local VM)
2. MYSQL + Apache + Wordpress 설치 
3. Plug-in 개발
4. SEO 홍보
5. 개발자 readme

### 1. Vagrant 설치 (local VM)
	
```
brew cask install virtualbox
brew cask install vagrant
```

설치할 directory 등 생성..

```
vim Vagrantfile
...
========================
Vagrant.configure("2") do |config|
	config.vm.box = "ubuntu/xenial64"  #"bento/ubuntu-16.04"
	config.vm.network "public_network"
# (config.vm.provision :shell, path: "wordpress-install.sh")
end
========================
...
vagrant up
vagrant ssh
```


access user setting
```
sudo passwd ubuntu
lsb_release -a
```

### 2. MYSQL + Apache + Wordpress 설치

```
sudo apt-get update
sudo apt-get install -y mysql-server
sudo mysql_secure_installation
```


setup mysql...
```
CREATE DATABASE `$DB_NAME` CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE user '$USER_NAME'@localhost identified by '$PASSWORD';
GRANT ALL ON $DB_NAME.* TO '$USER_NAME'@'localhost';
GRANT ALL PRIVILEGES ON $DB_NAME.* TO '$USER_NAME'@'localhost' IDENTIFIED BY '$PASSWORD';
FLUSH PRIVILEGES;
EXIT;
```


setup apache
```
sudo apt-get install -y apache2
```


apache config 파일 수정...

/etc/apache2/apache2.conf
(wordpress directory 열어주기)

```
<Directory /$path... >
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
</Directory>
```


/etc/apache2/ports.conf
사용할 포트 관리 (일반적인 경우엔 수정할필요가 거의 없음..)

/etc/apache2/sites-available/000-default.conf
virtual host 세팅 (아래는 http -> https 자동 redirect + https -> wordpress)

```
# http redirect to https
<VirtualHost *:80>
        RewriteEngine On
        RewriteRule ^(.*)$ https://%{HTTP_HOST}$1 [R=301,L]
</VirtualHost>

# mapping to wordpress
<VirtualHost *:443>
        SSLEngine on
        SSLCertificateFile /etc/apache2/ssl/6206aa88bd08fd66.crt
        SSLCertificateKeyFile /etc/apache2/ssl/kewtea.key
        SSLCertificateChainFile /etc/apache2/ssl/gd_bundle-g2-g1.crt

        ServerAdmin webmaster@localhost
        DocumentRoot /home/sangsoolee/wordpress
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```


virtual host 에서 사용한 mod 들 추가해서 적용

```
sudo a2enmod rewrite
```


변경사항 적용

```
sudo systemctl restart apache2.service
```


set up wp

```
sudo apt-get install -y build-essential libssl-dev libffi-dev python3-dev
sudo apt-get update
```

#### A. php7.0 설치 on ubuntu

```	
sudo apt-get install php7.0 php7.0-mysql libapache2-mod-php7.0 php7.0-cli php7.0-cgi php7.0-gd
sudo systemctl restart php7.0-fpm
```


#### B. php7.0 설치 on debian

```
sudo -s
echo 'deb http://packages.dotdeb.org jessie all' >> /etc/apt/sources.list
echo 'deb-src http://packages.dotdeb.org jessie all' >> /etc/apt/sources.list

sudo apt-get install git
cd /tmp
git clone https://github.com/kasparsd/php-7-debian.git
cd php-7-debian/
./build.sh

cd /tmp
wget https://www.dotdeb.org/dotdeb.gpg
sudo apt-key add dotdeb.gpg
rm dotdeb.gpg

sudo apt-get update
apt-cache search php7.0-\*

sudo apt-get install php7.0 php7.0-mysql libapache2-mod-php7.0 php7.0-cli php7.0-cgi php7.0-gd
sudo systemctl restart php7.0-fpm (안되면 install 먼저해보고 실행)
```


php 설치 완료후,

```
wget https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp
wp --info

# 워드프레스 설치할 디렉토리로 이동 ... (cd $wordpress_path)
sudo usermod -aG www-data $USER
sudo chown -R www-data /home/ubuntu/wordpress1
sudo chmod -R 755 /home/ubuntu/wordpress1
sudo -u www-data wp core download
sudo -u www-data wp core config --dbname=$DB_NAME --dbuser=$USER_NAME --dbpass=$PASSWORD --dbhost=localhost --dbprefix=wp_
sudo -u www-data wp core install --url="$SITE_URL" --title="$SITE_TITLE" --admin_user="$ADMIN_NAME" --admin_password="$ADMIN_PASSOWRD" --admin_email="$ADMIN_EMAIL"

# etc) wp cli 사용해보기 ...
sudo -u www-data wp plugin install woocommerce
wp plugin list
wp plugin activate woocommerce	
```

wp-config.php 에 사이트 경로 설정 (무한리다이렉트 문제) - gcp: 외부 ip 
```
define( 'WP_HOME', 'http://example.com' );
define( 'WP_SITEURL', 'http://example.com' );
```

august.zip 사용해보기
august 작업폴더(/src/plugins/august) 로 이동후 'zip -r ~/august.zip ./'
```
cd $wordpress_path
sudo -u www-data wp plugin install ../august.zip
```


몇개 기본 필수 plugin 설치
```
sudo -u www-data wp plugin install toggle-wpautop
sudo -u www-data wp plugin install wordpress-seo
sudo -u www-data wp plugin install woocommerce
wp plugin activate toggle-wpautop wordpress-seo woocommerce woocommerce-multilingual
```

WPML 은 [wpml.org](https://wpml.org/)을 통해 직접 설치
*activate 후 에러가 나올경우 '/var/log/apache2/error.log' 등을 통해 에러확인하여 고쳐야할수있음(대부분 모듈 추가설치의 이슈)*

### 3. Plug-in

1. August - Shortcode
2. DB Access (gSheet 연동...)
	
### 3-1. August - Shortcode
*attribute 작성시, 홀따음표(')로 작성할것*

####0) size
```
#width (px or %)
[vbox .. size='w100%' .. ]

#height
[vbox .. size='h300px' .. ]

#width & height
[vbox .. size='w100%h300px' .. ]
```

####1) unibox
```
#unibox - list
[vbox style='unibox' unibox_type='list' content='{
	"image": "http://image...",
	"desc": "Lorem ipsum dolor ...",
	"link": "http://..."
}']

#unibox - grid_n (1,2,3,4)
[vbox style='unibox' unibox_type='grid_1' content='{
	"image": "http://image...",
	"title": "August Unibox",
	"subtitle": "lorem ipsum dolor ...",
	"link": "http://..."
}']

[vbox style='unibox' unibox_type='grid_2' content='{ ... }']
[vbox style='unibox' unibox_type='grid_2' content='{ ... }']

[vbox style='unibox' unibox_type='grid_3' content='{ ... }']
[vbox style='unibox' unibox_type='grid_3' content='{ ... }']
[vbox style='unibox' unibox_type='grid_3' content='{ ... }']

[vbox style='unibox' unibox_type='grid_4' content='{ ... }']
[vbox style='unibox' unibox_type='grid_4' content='{ ... }']
[vbox style='unibox' unibox_type='grid_4' content='{ ... }']
[vbox style='unibox' unibox_type='grid_4' content='{ ... }']
```

####2) video
```
[vbox style='video' content='{
	"title": "August Video",
	"subtitle": "lorem ipsum",
	"image": "http://...",
	"link": "http://...",
	"webm": "http://...",
	"mp4": "http://...",
	"ogg": "http://..."
}'] 
```

####3) slide
```
[vbox style='slide' slide_type='horizontal' slide_interval='3000'
content='{"title": "Lorem Ipsum Qua", "link": "http://...", "image": "http://..."},
		 {"image": "http://..."},
		 ...'
 ]
#content={},{}... 로 여러개의 슬라이드장을 넣을 수 있음 
```

####4) append css
```
[vbox style='css' content='{
	"no-padding-container": true,
	"body-color": "#333",
	"banner-margin": "10px"
}']
```


### 4. SEO 홍보
...

### 5. 개발자 Read me

```
cd /vm/vm1/
vagrant up
vagrant ssh
ifconfig
# 여기서 나오는 ip 주소로 접속하면 됨
...
# wordpres 접속정보
# wpadmin / 1234
```

```
#에러검증코드
echo json_last_error();
switch (json_last_error()) {
	case JSON_ERROR_NONE:
		echo ' - No errors';
		break;
	case JSON_ERROR_DEPTH:
		echo ' - Maximum stack depth exceeded';
		break;
	case JSON_ERROR_STATE_MISMATCH:
		echo ' - Underflow or the modes mismatch';
		break;
	case JSON_ERROR_CTRL_CHAR:
		echo ' - Unexpected control character found';
		break;
	case JSON_ERROR_SYNTAX:
		echo ' - Syntax error, malformed JSON';
		break;
	case JSON_ERROR_UTF8:
		echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
		break;
	default:
		echo ' - Unknown error';
		break;
}
```

