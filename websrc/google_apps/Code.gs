/*
 메뉴를 눌러서 사이드 노트가 나온다
 사이드 노트에서 현재 tab_sheet를 어떤 template로 할지 정한다 (requests_config or result/report)
  - request type인 경우는 값들을 입력하고, 옆에서 자동으로 validate해서 다 되면 upload한다 
  - report type인 경우는 원하는 필드들을 없애서 단순화 하고, download(update)해서 값들이 출력된다
*/

var passPhrase = "1234";
var apiUrl = "http://3d377040.ngrok.io/rest/?pass_phrase="+passPhrase;

function onOpen() {
  var ui = SpreadsheetApp.getUi();
  ui.createMenu('August Data maanager')
  .addItem('Show sidebar', 'showSidebar')
  .addToUi();
}

function showSidebar() {
  var html = HtmlService.createHtmlOutputFromFile('Page')
      .setTitle('August Data manager')
      .setWidth(300);
  SpreadsheetApp.getUi() // Or DocumentApp or FormApp.
      .showSidebar(html);
}

function requestUpdate(method, dataType) {
	Logger.log("requestUpdate", method, dataType);
	
	var ss = SpreadsheetApp.getActiveSpreadsheet();
    var sheet = ss.getActiveSheet();
    var ui = SpreadsheetApp.getUi();
    
	if (method == "get") {
		ui.alert('get data ... please wait');
		
		var response = getJsonFromAPI(
		      apiUrl+"&content_type="+dataType,
		      "get"
		    );
		
		// TODO ## 인 것 찾아서, 그 밑에만 수정하기 기능
	    // 현재는 단순하고 안전하게, clear 시켜버리고 새로 쓰게함
	    sheet.clear();
	    
	    if ("products" == dataType) {
	    	// 첫번째 row 는 headers
		    sheet.appendRow(
				['id',
		    	'name',
				'price',
				'regular_price',
				'sale_price',
				'status',
				'stock_quantity',
				'stock_status',
				'short_description',
				'description']
		    );
		  
		    // insert data
		    for (var i=0; i<response.length; i++) {
		    	var data = response[i];
		      
		    	var id = data['id'];
		    	var name = data['data']['name'];
		    	var price = data['data']['price'];
		    	var regular_price = data['data']['regular_price'];
		    	var sale_price = data['data']['sale_price'];
		    	var status = data['data']['status'];
		    	var stock_quantity = data['data']['stock_quantity'];
		    	var stock_status = data['data']['stock_status'];
		    	var short_desc = data['data']['short_description'];
		    	var desc = data['data']['description'];
		    	
		    	sheet.appendRow(
					[id,
		    		name,
		    		price,
		    		regular_price,
		    		sale_price,
		    		status,
		    		stock_quantity,
		    		stock_status,
		    		short_desc,
		    		desc]
		    	);
		    }
	    }
	    
	    if ("orders" == dataType) {
	    	// 첫번째 row 는 headers
		    sheet.appendRow(
				['id',
		    	'customer_note',
				'payment_method',
				'payment_method_title',
				'address_1',
				'address_2',
				'city',
				'compnay',
				'country',
				'first_name',
				'last_name',
				'postcode',
				'state',
				'status']
		    );
		  
		    // TODO
		    // insert data
		    for (var i=0; i<response.length; i++) {
		    	var data = response[i];
		      
		    	var id = data['id'];
		    	var name = data['data']['name'];
		    	// ...
		    	
		    	sheet.appendRow(
					[id,
		    		name,
		    		]
		    	);
		    }
		    // ...
		    
	    } 
	    
	    ui.alert('done');
	}
	
	if (method == "set") {
		ui.alert('set data ... please wait');
		
		// 1) row 별로 for loop
		var row = sheet.getLastRow();
		var column = sheet.getLastColumn();

		// row 1은 헤더자리이므로 두번째부터 시작...
		for (var i=2; i<=row; i++) {
			var values = [];
			for (var j=1; j<=column; j++) {
				var range = sheet.getRange(i,j);
				var value = range.getValue();
				values.push(value);
			}
			
			var id = values[0];
			var data = {};
			data['name'] = values[1];
			data['price'] = values[2];
			data['regular_price'] = values[3];
			data['sale_price'] = values[4];
			data['status'] = values[5];
			data['stock_quantity'] = values[6];
			data['stock_status'] = values[7];
			data['short_desc'] = values[8];
			data['short_desc'] = values[9];

			getJsonFromAPI(
				apiUrl+"&content_type="+dataType+"&id="+id,
				"post",
				data
			);
		}
		
		ui.alert('done');
		
	}
    
}

function getJsonFromAPI(url, method, payload) {
  var method=method;  //'post', default is 'get'
  var payload=payload;  //{'field':'value',.....
  
  var options = {
   'method' : method, 
   'contentType': 'application/json'
  };
  
  if (payload) {
	  options['payload'] = JSON.stringify(payload); 
  }
  
  var response  = UrlFetchApp.fetch(url, options);
  if (response) {
	  var json = JSON.parse(response);
	  return json;	  
  } else {
	  return;
  }
}