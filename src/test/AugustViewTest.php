<?php

//define('WP_USE_THEMES', false); //or false for faster loading

$wordpress_dir='/home/ubuntu/wordpress1'; //dirname(__FILE__,2).'/wordpress1';

echo "loading wp ..... \n";
require_once($wordpress_dir."/wp-load.php");  //wp-blog-header.php, rtrim (disable theme)
require_once($wordpress_dir."/wp-admin/includes/plugin.php");
require_once($wordpress_dir."/wp-admin/includes/template.php");
//require_once($wordpress_dir."/wp-admin/admin.php");  //wp-blog-header.php, rtrim (disable theme)
echo "all done! \n";
wp_list_pages();
echo "all done! \n";

require_once($wordpress_dir.'/wp-content/plugins/august/include/aug_adminview.php');

use PHPUnit\Framework\TestCase;

final class AugustViewTest extends TestCase {
	private $august_view;

	protected function setUp() {
		$this->august_view = new August_Setting_View();
	}

	public function testLoaded() {
		$this->assertNotEquals(null, $this->august_view);
	}
	
	public function testSetupFields() {
		
		$this->august_view->setup_sections_fields();
		$options = get_option('aug_visual_options');
		echo $options;
		
		// $options['product_redirect'] 값 등을 가져오게하기 (default 값을 세팅하게 해야할듯)
		/*
		add_settings_section(
			'aug_visual_section', // ID used to identify this section and with which to register options
			'Visual Options',     // Title to be displayed on the administration page
			false,                // Callback used to render the description of the section
			'aug_visual_options'  // Page on which to add this section of options
		);
		$options = get_option('aug_visual_options');
		echo $options;
		*/
		
		$this->assertEquals(null, null);
		
	}
	
	protected function tearDown() {
		$this->august_view= NULL;
	}
	
}
?>