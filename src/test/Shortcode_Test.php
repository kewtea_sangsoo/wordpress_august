<?php
$wordpress_dir='/home/ubuntu/wordpress1';

echo "loading wp ...\n";
require_once($wordpress_dir."/wp-load.php");
echo "wp loaded!\n";

require_once($wordpress_dir.'/wp-content/plugins/august/module/shortcode/Shortcode_Handler.php');
use PHPUnit\Framework\TestCase;

final class Shortcode_Test extends TestCase {
	private $shortcode_handler;

	protected function setUp() {
		$this->shortcode_handler= new Shortcode_Handler();
	}
	
	public function testLoaded() {
		$this->assertNotEquals(null, $this->shortcode_handler);
	}
	
	public function testSetupFields() {
		
		/*
		// [august style='video' .. ]
		$my_atts = array(
			'style' => 'video',
			'data'	=> '{ "title": "titleText", "webm": "https://www.w3schools.com/html/mov_bbb.webm" }'
		);
		*/
		
		// [august style='css' .. ]
		$my_atts = array(
				'style' => 'css',
				'data'	=> '{ "banner_margin": "10px" }'
				// 'data'	=> '{"no_padding_container": true, "banner_margin": "10px" }'
		);
		
		$result = $this->shortcode_handler->build_shortcode($my_atts);
		echo $result;
		
		// TODO 어떤식으로 검증을 해야하나 ..?
		$this->assertEquals(null, null);
		
	}
	
	protected function tearDown() {
		$this->shortcode_handler= NULL;
	}
	
}
?>