<?php

/////////////////////////////////////////////////////////////////

add_filter( 'woocommerce_product_tabs', 'august_product_tabs', 98);
function august_product_tabs($tabs) {
	global $product;
	$id = $product->id;
	
	$args = array ('post_type' => 'product', 'post_id' => $id);
	$comments = get_comments( $args );
	
	$check_product_review_count = count($comments);
	
	$tabs['reviews']['title'] = 'Reviews ('.$check_product_review_count.')';
	
	// 아무것도 안나오는 디자인 적용됨..
	// 추후 if 추가하여 여기서 조정이 가능함
	return array();
	
	// reviews 만 나오도록 함
	/*
	 return array (
	 'reviews' => $tabs['reviews']
	 );*/
	// return $tabs;
}

// remove short description
add_filter( 'woocommerce_short_description', 'august_short_description', 98);
function august_short_description($variable) {
	//$variable = '';
	return '';
}

// product long body
add_action( 'woocommerce_after_main_content', 'august_after_main_content', 98);
function august_after_main_content() {
	global $product;
	$desc = $product->description;
	
	$elem = '<div class="kt_product-desctiption">'.$desc.'</div>';
	
	echo $elem;
}

add_filter( 'woocommerce_loop_add_to_cart_link', 'august_redirect_cart' );
function august_redirect_cart($link, $product) {
	return '<div class="add-to-cart-button-outer">'.
				'<div class="add-to-cart-button-inner">'.
					'<div class="add-to-cart-button-inner2">'.$link.'</div>'.
				'</div>'.
			'</div>';
}