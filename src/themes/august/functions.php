<?php

/*
// Display 24 products per page. Goes in functions.php
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 8;' ), 20 );

add_filter( 'woocommerce_product_tabs', 'wp_woo_rename_reviews_tab', 98);
function wp_woo_rename_reviews_tab($tabs) {
    global $product;
    $id = $product->id;

    $args = array ('post_type' => 'product', 'post_id' => $id);
    $comments = get_comments( $args );

    $check_product_review_count = count($comments);

    $tabs['reviews']['title'] = 'Reviews('.$check_product_review_count.')';

    return $tabs;
}


remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );


add_filter('logout_url', 'wpse_44020_logout_redirect', 10, 2);
function wpse_44020_logout_redirect($logouturl, $redir) {
    return $logouturl . '&amp;redirect_to='.get_permalink();
}
*/

// 

/////////

// Display 24 products per page. Goes in functions.php
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 8;' ), 20 );

// product tabs
add_filter( 'woocommerce_product_tabs', 'august_product_tabs', 98);
function august_product_tabs($tabs) {
    global $product;
    $id = $product->id;
    
    $args = array ('post_type' => 'product', 'post_id' => $id);
    $comments = get_comments( $args );

    $check_product_review_count = count($comments);

    $tabs['reviews']['title'] = 'Reviews ('.$check_product_review_count.')';

    // 아무것도 안나오는 디자인 적용됨..
    // 추후 if 추가하여 여기서 조정이 가능함
    return array();
    
    // reviews 만 나오도록 함
    /*
    return array (
    		'reviews' => $tabs['reviews']
    );*/
    // return $tabs;
}

// remove short description
add_filter( 'woocommerce_short_description', 'august_short_description', 98);
function august_short_description($variable) {
	//$variable = '';
	return '';	
}

// product thumbnails
/*
function woocommerce_show_product_thumbnails() {
	global $product;
	$img_ids = $product->gallery_image_ids;
	
	for ($i=0; $i<sizeof($img_ids); $i++) {
		$img_id = $img_ids[$i];
	}
	
	<div class="thumbnails columns-3">
		<a href="http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery1.jpg" class="zoom first" title="" data-rel="prettyPhoto[product-gallery]">
			<img width="180" height="180" src="http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery1-180x180.jpg" class="attachment-shop_thumbnail size-shop_thumbnail" alt="pr-gallery1" title="pr-gallery1" srcset="http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery1-150x150.jpg 150w, http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery1-180x180.jpg 180w, http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery1-300x300.jpg 300w, http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery1-600x600.jpg 600w, http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery1-550x550.jpg 550w" sizes="(max-width: 180px) 100vw, 180px">
		</a><a href="http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery2.jpg" class="zoom" title="" data-rel="prettyPhoto[product-gallery]"><img width="180" height="180" src="http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery2-180x180.jpg" class="attachment-shop_thumbnail size-shop_thumbnail" alt="pr-gallery2" title="pr-gallery2" srcset="http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery2-150x150.jpg 150w, http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery2-180x180.jpg 180w, http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery2-300x300.jpg 300w, http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery2-600x600.jpg 600w, http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery2-550x550.jpg 550w" sizes="(max-width: 180px) 100vw, 180px"></a><a href="http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery3.jpg" class="zoom last" title="" data-rel="prettyPhoto[product-gallery]"><img width="180" height="180" src="http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery3-180x180.jpg" class="attachment-shop_thumbnail size-shop_thumbnail" alt="pr-gallery3" title="pr-gallery3" srcset="http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery3-150x150.jpg 150w, http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery3-180x180.jpg 180w, http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery3-300x300.jpg 300w, http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery3-600x600.jpg 600w, http://creationground.com/wp-content/uploads/sites/2/2015/03/pr-gallery3-550x550.jpg 550w" sizes="(max-width: 180px) 100vw, 180px"></a></div>
	
}
*/

// product long body
add_action( 'woocommerce_after_main_content', 'august_after_main_content', 98);
function august_after_main_content() {
	global $product;
	$desc = $product->description;
	
	$elem = '<div class="kt_product-desctiption">'.$desc.'</div>';
	
	echo $elem;
}

//
add_filter( 'woocommerce_loop_add_to_cart_link', 'august_redirect_cart' );
function august_redirect_cart($link, $product) {
	return '<div class="add-to-cart-button-outer">'.
				'<div class="add-to-cart-button-inner">'.
					'<div class="add-to-cart-button-inner2">'.$link.'</div>'.
				'</div>'.
			'</div>';
}
