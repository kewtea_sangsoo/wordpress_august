<!-- released 2017.07.17 -->
<!DOCTYPE html>
    <html <?php language_attributes(); ?>>
        <head>
            <meta charset="<?php bloginfo( 'charset' ); ?>">
            
        	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        	<meta http-equiv="cache-control" content="no-cache" />
        	<meta http-equiv="pragma" content="no-cache" />

            <meta name="apple-mobile-web-app-capable" content="yes" />
            
            <meta name="viewport" content="width=device-width, initial-scale=1" />

            <link href='https://fonts.googleapis.com/css?family=Raleway:600,400|Dancing+Script' rel='stylesheet' type='text/css'>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous" />
            <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/3rdparty/font-awesome.css">

            <?php wp_head(); ?>
            
            <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
            
        </head>
        <body <?php body_class(); ?>>
            <div id="wrapper" class="hfeed">

                <aside class="vertical_menu_area">
                    <div class="vertical_menu_area_inner">
                        <!-- logo -->
                        <div class="vertical_logo_wrapper">
                            <div class="eltd_logo_vertical" style="height: 65px;">
                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                                    <?php
                                    wp_get_attachment_image(
                                    	$logo_id,
                                    	$size,
                                    	false,
                                    	array(
                                    			'class'     => 'site-logo attachment-' . $size,
                                    			'data-size' => $size,
                                    			'itemprop'  => 'logo'
                                    	)
                                    )
                                    ?>
                                </a>
                            </div>
                        </div>
                        <!-- menu -->
                        <nav class="desktop-menu" id="menu" role="navigation">
                            <?php wp_nav_menu(); ?>
                        </nav>
                        <!-- widgets -->
                        <div id="primary" class="widget-area">
                            <div class="shopping_cart_outer">
                                <div class="shopping_cart_inner">
                                    <div class="shopping_cart_header">
                                        <a class="header_cart with_button" href="<?php echo esc_url( home_url( '/' ) ).'cart/'; ?>">
                                            <span class="cart_label">Cart</span>
                                            <span class="header_cart_span "><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                            			</a>
                                    </div>
                            	</div>
                            </div>
                        </div>
                    </div>
                </aside>

                <header id="header" class="page-header" role="banner">
                    <div class="header-container">
                        <div class="header-inner">
                            <div class="header_inner_left">
                                <div class="mobile_menu_button">
                                    <span><i class="fa fa-bars"></i></span>
                                </div>

                                <div class="logo_wrapper" style="height: 129px;">
                                    <div class="eltd_logo">
                                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" style="height: 129px; visibility: visible;">
                                            <?php
		                                    wp_get_attachment_image(
		                                    	$logo_id,
		                                    	$size,
		                                    	false,
		                                    	array(
		                                    			'class'     => 'site-logo attachment-' . $size,
		                                    			'data-size' => $size,
		                                    			'itemprop'  => 'logo'
		                                    	)
		                                    )
		                                    ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <nav id="menu" role="navigation" class="is-hide" style="height: 0">
                                <?php wp_nav_menu(); ?>
                            </nav>
                            <div class="shopping_cart_outer">
                                <div class="shopping_cart_inner">
                                    <div class="shopping_cart_header">
                                        <a class="header_cart with_button" href="<?php echo esc_url( home_url( '/' ) ).'cart/'; ?>">
                                            <i class="fa fa-shopping-cart"></i>
                                            <?php if (0 < WC()->cart->get_cart_contents_count()) : ?>
                                                <span class="header_cart_span "><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                                            <?php endif; ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

                <div class="paspartu_outer  paspartu_on_bottom_slider paspartu_on_bottom_fixed">
                    <div class="paspartu_middle_inner">
                        <div class="paspartu_top"></div>
                        <div class="paspartu_left"></div>
                        <div class="paspartu_right"></div>
                        <div class="paspartu_bottom"></div>
                        <div class="paspartu_inner">
                            <div id="container">
                                <div id="container-inner">