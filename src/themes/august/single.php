<?php get_header(); ?>
<style>
div#container {
    background-color: #fff !important;
}
</style>
<section id="content" role="main">
    <div class="post-detail blog_holder">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    			<div class="post_content_holder">
			        <div class="post-image">
			            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
			                <?php if ( has_post_thumbnail() ) the_post_thumbnail(); ?>
			            </a>
			        </div>
			
			        <div class="post-text">
			            <div class="post_text_inner">
			                <div class="post-info">
			                    <div class="date"><?php the_time( get_option( 'date_format' ) ); ?></div>
			                    <div class="post_category">
			                        <?php _e( 'in ', 'blankslate' ); ?><?php the_category( ', ' ); ?>
			                    </div>
			                    <div class="post_info_author_holder">
			                        by <?php the_author_posts_link(); ?>
			                    </div>
			                    <div class="post_comments_holder">
			                        <a class="post_comments" href="<?php the_permalink(); ?>#comments" target="_self">
			                            <?php comments_number(); ?>
			                        </a>
			                    </div>
			                </div>
			
			                <h2><?php the_title(); ?></h2>
			
			                <div class="post-body" style=" text-align:left;">
			                    <?php the_content(); ?>
			                </p>
			            </div>
			        </div>
			    </div>
			</article>

			<div class="single-tags">
    			<div class="tags_text">
        			<h6 class="tags-heading">Tags:</h6>
        			<?php the_tags('', '' ); ?>
    			</div>
			</div>

			<div class="author-description">
    			<div class="author-description-inner">
        			<div class="image">
            			<?php echo get_avatar(  get_the_author_meta( 'ID' ) ); ?>
        			</div>
        			<div class="author-text-holder">
            			<h6 class="author_name"><?php the_author(); ?></h6>
        			</div>
    			</div>
			</div>
            
            <?php if ( ! post_password_required() ) comments_template( '', true ); ?>
            <?php endwhile; endif; ?>
    </div>
</section>
<?php get_footer(); ?>