<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="post_content_holder">
        <div class="post-image">
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <?php if ( has_post_thumbnail() ) the_post_thumbnail(); ?>
            </a>
        </div>
        <div class="post-text">
            <div class="post_text_inner">

                <div class="post-info">
                    <div class="date"><?php the_time( get_option( 'date_format' ) ); ?></div>
                    <div class="post_category">
                        <?php _e( 'in ', 'august' ); ?><?php the_category( ', ' ); ?>
                    </div>
                    <div class="post_info_author_holder">
                        by <?php the_author_posts_link(); ?>
                    </div>
                </div>

                <h2>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        <?php the_title(); ?>
                    </a>
                </h2>

                <p class="post-excerpt">
                    <?php the_excerpt(); ?>
                </p>

                <div class="read-more-wrapper">
                    <a href="<?php the_permalink(); ?>" target="_self" class="read-more-btn">Read More</a>
                </div>

            </div>
        </div>
    </div>
</article>