<?php if ( 'comments.php' == basename( $_SERVER['SCRIPT_FILENAME'] ) ) return; ?>
<div class="comment-holder" id="comments">
    <div class="comment_number"><div class="comment_number_inner"><h6><?php comments_number(); ?></h6></div></div>
    <div class="comments">
        <ul class="comment-list">
            <?php wp_list_comments( 'type=comment&callback=my_comment' ); ?>
        </ul>
    </div>
</div>
<?php if ( comments_open() ) comment_form(); ?>