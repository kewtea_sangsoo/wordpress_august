							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
        
        <footer id="footer" class="footer-<?php echo ICL_LANGUAGE_CODE; ?>">
        	<div class="footer-wrapper">
        		<div class="footer-col company-info col-xs-12 col-sm-9 col-md-9 col-lg-9">
        			<?php if ('ko' ==  ICL_LANGUAGE_CODE) : ?>
        				<h5>주식회사 큐티코리아</h5>
                        서울시 강남구 강남대로 62길 34, 201 (역삼동) | 사업자등록번호: 220-88-45458 <a href="http://www.ftc.go.kr/info/bizinfo/communicationView.jsp?apv_perm_no=2015322016230202417&area1=&area2=&currpage=1&searchKey=04&searchVal=2208845458&stdate=&enddate=" target="_blank">사업자정보확인</a><br>
                        대표이사: 이정한 | 개인정보관리책임자: 이상수 | 통신판매업신고번호: 2015-서울강남-02417호<br>
                        <br>
                        tel: <a href="tel:02-564-4553">02-564-4553</a> | email: <a href="mailto:support@kewtea.com">support@kewtea.com</a>
                        <div class="footer-for-user">
                            <a href="/ko/policy/#terms" target="_blank">이용약관</a> | <a href="/ko/policy/#privacy_policy" target="_blank">개인정보보호정책</a> | <a href="/ko/policy/#returnandrefund" target="_blank">환불정책</a> | <a href="/ko/faq" target="_blank">FAQ</a>
                        </div>
                     <?php else : ?>
                        <h5 style="font-size: 28px; margin-bottom: 7px; color: #ECF0F1; line-height: initial;">
                            Spirit House
                        </h5>
                        <div class="footer-slogan">Sincerely hand-picked for you</div>
                        <div class="footer-for-user">
                        	<a href="/policy/#terms" target="_blank">Terms & Conditions</a> | <a href="/policy/#privacy_policy" target="_blank">Privacy Policy</a> | <a href="/policy/#returnandrefund" target="_blank">Return and Refund</a> | <a href="/faq" target="_blank">FAQ</a>
                        </div>
                     <?php endif ?>
                 </div>

                 <div class="footer-col footer-right col-xs-12 col-sm-3 col-md-3 col-lg-3">
                     <div class="follow-social">
                        <h5 class="social-title">Follow us</h5>
                        <span class="social-button">
                            <a href="https://www.facebook.com" target="_blank">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </span>
                        <span class="social-button">
                            <a href="https://www.instagram.com/" target="_blank">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </span>
                     </div>

                    <?php
                        $enter_email = 'Enter your email';
                        if ('ko' == ICL_LANGUAGE_CODE) $enter_email = '이메일을 입력해주세요';
                    ?>

                     <div class="subscribe-wrapper">
                        <h5 class="social-title">Newsletter</h5>
                        <div id="kt_footer_emailWrap">
                            <form action="https://kewtea.us8.list-manage.com/subscribe/post?u=5009954b3ff18233d85cb9a5c&amp;id=421b5e6ec9" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
                                <div class="mc-field-group">
                                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="<?php echo $enter_email; ?>"></div>
                                    <div style="display: none;"><input type="text" name="b_5009954b3ff18233d85cb9a5c_421b5e6ec9" tabindex="-1" value=""></div>
                                    <div class="clear" style="/* width: 45px; */">
                                        <input type="submit" value="OK" name="subscribe" id="mc-embedded-subscribe" class="button" style="width: 45px; padding: 4.5px;">
                                    </div>
                            </form>
                        </div>
                     </div>

                     <div class="lang-sel-wrapper">
                         <div class="lang-sel-text">Choose your country</div>
                        <div class="lang-sel-dropdown">
                            <?php global $sitepress; ?>
                            <?php if ($sitepress) :?>
	                            <?php if ('ko' ==  ICL_LANGUAGE_CODE) : ?>
	                                <?php $lang_url = $sitepress->language_url( 'en' ); ?>
	                                <div class="dropdown">
	                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
	                                        한국 스토어
	                                        <span class="caret"></span>
	                                    </button>
	                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
	                                        <li><a href="<?php echo $lang_url; ?>">International Store</a></li>
	                                    </ul>
	                                </div>
	                            <?php else : ?>
	                                <?php $lang_url = $sitepress->language_url( 'ko' ); ?>
	                                <div class="dropdown">
	                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
	                                        International Store
	                                        <span class="caret"></span>
	                                    </button>
	                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
	                                        <li><a href="<?php echo $lang_url; ?>">한국 스토어</a></li>
	                                    </ul>
	                                </div>
	                            <?php endif ?>
                            <?php endif ?>
                        </div>
                     </div>

                 </div>
				
                 <div class="copyright">2016 © Kewtea</div>

             </div>
         </footer>

		<?php wp_footer(); ?>
         
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
            integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
            crossorigin="anonymous"></script>

		<script type="text/javascript">
		    	'use strict';
		    	$ = jQuery;
		
		        var isMobile = false; //initiate as false
		        // device detection
		        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
		            || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
		
		        if (isMobile) {
		            jQuery('body').addClass("is-mobile");
		
		            /*! jQuery Mobile v1.3.2 | Copyright 2010, 2013 jQuery Foundation, Inc. | jquery.org/license */
		            (function(e,t,n){typeof define=="function"&&define.amd?define(["jquery"],function(r){return n(r,e,t),r.mobile}):n(e.jQuery,e,t)})(this,document,function(e,t,n,r){(function(e,t,n,r){function x(e){while(e&&typeof e.originalEvent!="undefined")e=e.originalEvent;return e}function T(t,n){var i=t.type,s,o,a,l,c,h,p,d,v;t=e.Event(t),t.type=n,s=t.originalEvent,o=e.event.props,i.search(/^(mouse|click)/)>-1&&(o=f);if(s)for(p=o.length,l;p;)l=o[--p],t[l]=s[l];i.search(/mouse(down|up)|click/)>-1&&!t.which&&(t.which=1);if(i.search(/^touch/)!==-1){a=x(s),i=a.touches,c=a.changedTouches,h=i&&i.length?i[0]:c&&c.length?c[0]:r;if(h)for(d=0,v=u.length;d<v;d++)l=u[d],t[l]=h[l]}return t}function N(t){var n={},r,s;while(t){r=e.data(t,i);for(s in r)r[s]&&(n[s]=n.hasVirtualBinding=!0);t=t.parentNode}return n}function C(t,n){var r;while(t){r=e.data(t,i);if(r&&(!n||r[n]))return t;t=t.parentNode}return null}function k(){g=!1}function L(){g=!0}function A(){E=0,v.length=0,m=!1,L()}function O(){k()}function M(){_(),c=setTimeout(function(){c=0,A()},e.vmouse.resetTimerDuration)}function _(){c&&(clearTimeout(c),c=0)}function D(t,n,r){var i;if(r&&r[t]||!r&&C(n.target,t))i=T(n,t),e(n.target).trigger(i);return i}function P(t){var n=e.data(t.target,s);if(!m&&(!E||E!==n)){var r=D("v"+t.type,t);r&&(r.isDefaultPrevented()&&t.preventDefault(),r.isPropagationStopped()&&t.stopPropagation(),r.isImmediatePropagationStopped()&&t.stopImmediatePropagation())}}function H(t){var n=x(t).touches,r,i;if(n&&n.length===1){r=t.target,i=N(r);if(i.hasVirtualBinding){E=w++,e.data(r,s,E),_(),O(),d=!1;var o=x(t).touches[0];h=o.pageX,p=o.pageY,D("vmouseover",t,i),D("vmousedown",t,i)}}}function B(e){if(g)return;d||D("vmousecancel",e,N(e.target)),d=!0,M()}function j(t){if(g)return;var n=x(t).touches[0],r=d,i=e.vmouse.moveDistanceThreshold,s=N(t.target);d=d||Math.abs(n.pageX-h)>i||Math.abs(n.pageY-p)>i,d&&!r&&D("vmousecancel",t,s),D("vmousemove",t,s),M()}function F(e){if(g)return;L();var t=N(e.target),n;D("vmouseup",e,t);if(!d){var r=D("vclick",e,t);r&&r.isDefaultPrevented()&&(n=x(e).changedTouches[0],v.push({touchID:E,x:n.clientX,y:n.clientY}),m=!0)}D("vmouseout",e,t),d=!1,M()}function I(t){var n=e.data(t,i),r;if(n)for(r in n)if(n[r])return!0;return!1}function q(){}function R(t){var n=t.substr(1);return{setup:function(r,s){I(this)||e.data(this,i,{});var o=e.data(this,i);o[t]=!0,l[t]=(l[t]||0)+1,l[t]===1&&b.bind(n,P),e(this).bind(n,q),y&&(l.touchstart=(l.touchstart||0)+1,l.touchstart===1&&b.bind("touchstart",H).bind("touchend",F).bind("touchmove",j).bind("scroll",B))},teardown:function(r,s){--l[t],l[t]||b.unbind(n,P),y&&(--l.touchstart,l.touchstart||b.unbind("touchstart",H).unbind("touchmove",j).unbind("touchend",F).unbind("scroll",B));var o=e(this),u=e.data(this,i);u&&(u[t]=!1),o.unbind(n,q),I(this)||o.removeData(i)}}}var i="virtualMouseBindings",s="virtualTouchID",o="vmouseover vmousedown vmousemove vmouseup vclick vmouseout vmousecancel".split(" "),u="clientX clientY pageX pageY screenX screenY".split(" "),a=e.event.mouseHooks?e.event.mouseHooks.props:[],f=e.event.props.concat(a),l={},c=0,h=0,p=0,d=!1,v=[],m=!1,g=!1,y="addEventListener"in n,b=e(n),w=1,E=0,S;e.vmouse={moveDistanceThreshold:10,clickDistanceThreshold:10,resetTimerDuration:1500};for(var U=0;U<o.length;U++)e.event.special[o[U]]=R(o[U]);y&&n.addEventListener("click",function(t){var n=v.length,r=t.target,i,o,u,a,f,l;if(n){i=t.clientX,o=t.clientY,S=e.vmouse.clickDistanceThreshold,u=r;while(u){for(a=0;a<n;a++){f=v[a],l=0;if(u===r&&Math.abs(f.x-i)<S&&Math.abs(f.y-o)<S||e.data(u,s)===f.touchID){t.preventDefault(),t.stopPropagation();return}}u=u.parentNode}}},!0)})(e,t,n),function(e){e.mobile={}}(e),function(e,t){var r={touch:"ontouchend"in n};e.mobile.support=e.mobile.support||{},e.extend(e.support,r),e.extend(e.mobile.support,r)}(e),function(e,t,r){function l(t,n,r){var i=r.type;r.type=n,e.event.dispatch.call(t,r),r.type=i}var i=e(n);e.each("touchstart touchmove touchend tap taphold swipe swipeleft swiperight scrollstart scrollstop".split(" "),function(t,n){e.fn[n]=function(e){return e?this.bind(n,e):this.trigger(n)},e.attrFn&&(e.attrFn[n]=!0)});var s=e.mobile.support.touch,o="touchmove scroll",u=s?"touchstart":"mousedown",a=s?"touchend":"mouseup",f=s?"touchmove":"mousemove";e.event.special.scrollstart={enabled:!0,setup:function(){function s(e,n){r=n,l(t,r?"scrollstart":"scrollstop",e)}var t=this,n=e(t),r,i;n.bind(o,function(t){if(!e.event.special.scrollstart.enabled)return;r||s(t,!0),clearTimeout(i),i=setTimeout(function(){s(t,!1)},50)})}},e.event.special.tap={tapholdThreshold:750,setup:function(){var t=this,n=e(t);n.bind("vmousedown",function(r){function a(){clearTimeout(u)}function f(){a(),n.unbind("vclick",c).unbind("vmouseup",a),i.unbind("vmousecancel",f)}function c(e){f(),s===e.target&&l(t,"tap",e)}if(r.which&&r.which!==1)return!1;var s=r.target,o=r.originalEvent,u;n.bind("vmouseup",a).bind("vclick",c),i.bind("vmousecancel",f),u=setTimeout(function(){l(t,"taphold",e.Event("taphold",{target:s}))},e.event.special.tap.tapholdThreshold)})}},e.event.special.swipe={scrollSupressionThreshold:30,durationThreshold:1e3,horizontalDistanceThreshold:30,verticalDistanceThreshold:75,start:function(t){var n=t.originalEvent.touches?t.originalEvent.touches[0]:t;return{time:(new Date).getTime(),coords:[n.pageX,n.pageY],origin:e(t.target)}},stop:function(e){var t=e.originalEvent.touches?e.originalEvent.touches[0]:e;return{time:(new Date).getTime(),coords:[t.pageX,t.pageY]}},handleSwipe:function(t,n){n.time-t.time<e.event.special.swipe.durationThreshold&&Math.abs(t.coords[0]-n.coords[0])>e.event.special.swipe.horizontalDistanceThreshold&&Math.abs(t.coords[1]-n.coords[1])<e.event.special.swipe.verticalDistanceThreshold&&t.origin.trigger("swipe").trigger(t.coords[0]>n.coords[0]?"swipeleft":"swiperight")},setup:function(){var t=this,n=e(t);n.bind(u,function(t){function o(t){if(!i)return;s=e.event.special.swipe.stop(t),Math.abs(i.coords[0]-s.coords[0])>e.event.special.swipe.scrollSupressionThreshold&&t.preventDefault()}var i=e.event.special.swipe.start(t),s;n.bind(f,o).one(a,function(){n.unbind(f,o),i&&s&&e.event.special.swipe.handleSwipe(i,s),i=s=r})})}},e.each({scrollstop:"scrollstart",taphold:"tap",swipeleft:"swipe",swiperight:"swipe"},function(t,n){e.event.special[t]={setup:function(){e(this).bind(n,e.noop)}}})}(e,this)});
		
		            $(document).ready(function() {
		                $(".kt-slider.carousel.horizontal").swiperight(function() {
		                    $(this).carousel('prev');
		                });
		                $(".kt-slider.carousel.horizontal").swipeleft(function() {
		                    $(this).carousel('next');
		                });
		            });
		        }

		        function resizeContainer() {
		            var width = jQuery(window).width();
		            var height = jQuery(window).height();
		            	height -= (2*(width*0.02));
		            if (isMobile && "0px" != jQuery("div#container").css("min-height")) return;
		            jQuery("div#container").css("min-height", height);
		        }
		        
		        var menuHeight = 315;
		        function getMobileMenuHeight() {
		            menuHeight = jQuery(".page-header nav#menu > div").height();
		        }
		        
		        jQuery(window).resize(function() {
		            resizeContainer();
		            getMobileMenuHeight();
		        });
		        resizeContainer();
		        getMobileMenuHeight();
		
		        var isRunning = false;
		        jQuery(".mobile_menu_button").click(function() {
		            if (!isRunning) {
		                isRunning = true;
		                var isHide = jQuery(".page-header nav#menu").hasClass("is-hide");
		                var heightTo = 0;
		                if (isHide) {
		                    jQuery(".page-header nav#menu").removeClass("is-hide");
		                    heightTo = menuHeight;
		                } else {
		                    jQuery(".page-header nav#menu").addClass("is-hide");
		                    heightTo = 0;
		                }
		
		                jQuery(".page-header nav#menu").animate({
		                    height: heightTo
		                }, 500, function() {
		                    isRunning = false;
		                });
		            }
		        });
		
		        function quantityPlus(_this) {
		            var maxNum = $(_this).parent().find(".input-text").prop("max");
		            var num = $(_this).parent().find(".input-text").val();
		            num++;
		            if (maxNum < num) num = maxNum;
		            $(_this).parent().find(".input-text").val(num);
		        }
		        function quantityMinus(_this) {
		            var minNum = $(_this).parent().find(".input-text").prop("min");
		            var num = $(_this).parent().find(".input-text").val();
		            num--;
		            if (minNum > num) num = minNum;
		            $(_this).parent().find(".input-text").val(num);
		        }
		
		        $("input#createaccount").change(function(){
		            var isChecked = $("input#createaccount").is(":checked");
		            if (isChecked) $("button#place_order").html("Place Order<br>with create account");
		            else $("button#place_order").html("Place Order");
		        });
		
		    </script>
    
    </body>
</html>