<?php get_header(); ?>
<style>
div#container {
    background-color: #fff !important;
}
</style>
<section id="content" role="main">
    <div class="blog-standard blog_holder blog_standard_type">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'entry' ); ?>
            <?php comments_template(); ?>
        <?php endwhile; endif; ?>
        <?php get_template_part( 'nav', 'below' ); ?>
    </div>
</section>
<?php // get_sidebar(); ?>
<?php get_footer(); ?>