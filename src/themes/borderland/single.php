<?php get_header(); ?>
<style>
div#container { background-color: #fff !important; }
.woocommerce ul.products li.product,
.woocommerce-page ul.products li.product {
    border: 1px solid #eee;
}
</style>
<section id="content" role="main">
    <div class="post-detail blog_holder">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'post-entry' ); ?>
            <?php if ( ! post_password_required() ) comments_template( '', true ); ?>
            <?php endwhile; endif; ?>
    </div>
</section>
<?php // get_sidebar(); ?>
<?php get_footer(); ?>