<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $eltd_options;

$products_info_style_type = "accordions";
if(isset($eltd_options['woo_products_info_style'])) {
	$products_info_style_type = $eltd_options['woo_products_info_style'];
}
?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div class="product-holder">
<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="single_product_image_wrapper">
		<?php
			/**
			 * woocommerce_before_single_product_summary hook
			 *
			 * @hooked woocommerce_show_product_sale_flash - 10
			 * @hooked woocommerce_show_product_images - 20
			 */
			do_action( 'woocommerce_before_single_product_summary' );
		?>
	</div>

	<div class="summary entry-summary">
        <div class="clearfix summary-inner">
			<?php
                /**
                 * woocommerce_single_product_summary hook
                 *
                 * @hooked woocommerce_template_single_title - 5
                 * @hooked woocommerce_template_single_price - 10
                 * @hooked woocommerce_template_single_excerpt - 20
                 * @hooked woocommerce_template_single_add_to_cart - 30
                 * @hooked woocommerce_template_single_meta - 40
                 * @hooked woocommerce_template_single_sharing - 50
                 */
                do_action( 'woocommerce_single_product_summary' );
			?>
		</div><!-- .summary-inner -->
		<?php do_action( 'woocommerce_after_single_product_summary' ); ?>
	</div><!-- .summary -->
	
	<?php do_action( 'my_after_single_product_summary' );?>
	
	<?php
	   global $product, $woocommerce_loop;

       $related = $product->get_related( $posts_per_page );
       if ( sizeof( $related ) == 0 ) return;

       $args = apply_filters( 'woocommerce_related_products_args', array(
            'post_type'				=> 'product',
            'ignore_sticky_posts'	=> 1,
            'no_found_rows' 		=> 1,
            'posts_per_page' 		=> 4,
            'orderby' 				=> $orderby,
            'post__in' 				=> $related,
            'post__not_in'			=> array( $product->id )
       ) );

       $products = new WP_Query( $args );

       $woocommerce_loop['columns'] = $columns;

       if ( $products->have_posts() ) : ?>

       	<div class="related products">

       		<h4 class="related-products-title"><?php _e( 'Related Products', 'woocommerce' ); ?></h4>

       		<?php woocommerce_product_loop_start(); ?>

       			<?php while ( $products->have_posts() ) : $products->the_post(); ?>

       				<?php wc_get_template_part( 'content', 'product' ); ?>

       			<?php endwhile; // end of the loop. ?>

       		<?php woocommerce_product_loop_end(); ?>

       	</div>

       <?php endif; ?>
       
       <?php wp_reset_postdata(); ?>

	</div>
	</div><!-- #product-<?php the_ID(); ?> -->

	<meta itemprop="url" content="<?php the_permalink(); ?>" />


<?php do_action( 'woocommerce_after_single_product' ); ?>

<script type="text/javascript">
// 아코디언 ( Reviews(n) 탭 클릭 이벤트 ... )
$ = jQuery;

$(".accordion_content").addClass("is-hide");

$(".title-holder").click(function() {
    console.log($(this));
    var content = $(this).next();
    var heightTo = 0;

    if (content.hasClass("is-hide")) {
        heightTo = content.find(".accordion_content_inner").outerHeight();
        console.log(heightTo);
    }

    $(this).toggleClass("is-select");
    content.toggleClass("is-hide");

    content.animate({
        height: heightTo
    }, 400);

});
</script>
