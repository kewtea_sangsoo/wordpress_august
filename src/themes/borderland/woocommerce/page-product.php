<?php
    /* Template Name: Product page */

    if ( ! defined( 'ABSPATH' ) ) {
        exit; // Exit if accessed directly
    }

    get_header( 'shop' );
?>
<script src="<?php echo get_stylesheet_directory_uri();?>/3rdparty/lory.min.js"></script>
<style>
    div#container-inner {
        width: 100%;
        padding: 0;
    }
    div#container { background-color: #222 !important; }

    .slider {
        position: relative;
        width: 100%;
        -webkit-user-select: none;
        -moz-user-select: -moz-none;
        user-select: none;
        margin-bottom: 2%;
        overflow: hidden;
    }

    .frame,
    .events_log {
        position: relative;
        margin: 0 auto;
        font-size: 0;
        line-height: 0;
        overflow: visible;
        white-space: nowrap;
    }

    .frame li {
        position: relative;
        display: inline-block;
        width: 100%; height: 130px;
        text-align: center;
        font-size: 15px;
        line-height: 30px;
        color: #fff;
        overflow: hidden;
    }

    .slides { display: inline-block; }
    .percentage .slides { display: block; }
    .percentage .frame { width: 100%; }
    .percentage li { width: 100%; }
    .variablewidth li {
        width: 280px;
        margin-right: 10px;
    }
    .multipleelements li,
    .multislides li,
    .ease li {
        width: 60px;
        margin-right: 10px;
    }
    .simple li:last-child,
    .rewind li:last-child,
    .events li:last-child,
    .variablewidth li:last-child,
    .multipleelements li:last-child,
    .multislides li:last-child,
    .ease li:last-child { margin-right: 0; }
    .prev, .next {
        position: absolute;
        top: 50%;
        margin-top: -25px;
        display: block;
        cursor: pointer;
    }
    .next { right: 0; }
    .prev { left: 0; }
    .next svg, .prev svg { width: 25px; }

    .woocommerce .products ul,
    .woocommerce ul.products {
        margin-bottom: 0 !important;
    }

    /* 임시 컬러 */
    .slider .slide-glass {
        position: absolute;
        width: 100%; height: 100%;
        left: 0; top: 0;
    }
    .slider .slide-image {
        width: 100%; height: 100%;
        background-position: center center;
        background-size: cover;
    }
    .slider .slide-description {
        position: absolute;
        padding: 5px 10px;
        left: 0;
        bottom: 0;
        background-color: rgba(0,0,0,0.6);
    }

    .product-post-outer {
        position: relative;
        width: 100%; height: 270px;
        margin-bottom: 2%;
    }

    .product-post-outer .post-image {
        position: absolute;
        width: 100%; height: 100%;
        left: 0; top: 0;
        background-position: center center;
        background-size: cover;
    }

    .product-post-outer .post-title {
        position: absolute;
        width: 100%;
        bottom: 0;
        padding: 40px 20px 20px;
        font-size: 28px;
        color: #fff;
        font-weight: 700;
        letter-spacing: 3px;
        background: -webkit-linear-gradient(rgba(0,0,0,0), rgba(0,0,0,0.6)); /* For Safari 5.1 to 6.0 */
        background: -o-linear-gradient(rgba(0,0,0,0), rgba(0,0,0,0.6)); /* For Opera 11.1 to 12.0 */
        background: -moz-linear-gradient(rgba(0,0,0,0), rgba(0,0,0,0.6)); /* For Firefox 3.6 to 15 */
        background: linear-gradient(rgba(0,0,0,0), rgba(0,0,0,0.6)); /* Standard syntax */
    }
</style>
<?php if ( have_posts() ) : ?>
    <section class="entry-content woocommerce-page">
        <?php woocommerce_content(); ?>
        <?php the_content(); ?>
    </section>
<?php endif; ?>

<?php get_footer( 'shop' ); ?>

<script type="text/javascript">
	'use strict';
	$ = jQuery;

	var myLory = {};
	var dummyWidth = $("#container").width();
    var dummyHeight = $("#container").height();

    // document loaded
    if (0 < $(".js_simple").length) {
        document.addEventListener('DOMContentLoaded', function () {
            initLory();
        });

        // resize event
        $(window).on('load', function () {
            $(this).trigger('resize');
        });

        $(window).resize(function() {
            dummyWidth = $("#container").width();
            dummyHeight = $("#container").height();
            resizeLory();
        });
    }

    function initLory() {
        var simple = document.querySelector('.js_simple');

        $(".js_simple").width(dummyWidth);
        $(".js_simple .frame").width(dummyWidth);
        $(".js_simple li.js_slide").width(dummyWidth);

        var slideNumber = $(simple).find("li.js_slide").length;

        // setup lory
        myLory = lory(simple, {
            infinite: 1,
            enableMouseEvents: true,
            slideSpeed: 450
        });
        myLory.slideNumber = slideNumber;

        $(".js_simple li.js_slide").height(500);

        // set navigation (custom)
        // $(".slide-navigation").loryNavi(myLory, simple);

        // $($("#about-kew .slide-image")[0]).asyncBackgroundImage("#666");
    }

    function resizeLory() {
        $(".js_simple").width(dummyWidth);
        $(".js_simple .frame").width(dummyWidth);
        $(".js_simple li.js_slide").width(dummyWidth);
    }
</script>
