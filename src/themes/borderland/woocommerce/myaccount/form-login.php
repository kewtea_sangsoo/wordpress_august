<?php
/**
 * Login Form
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.2.6
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

    <div class="col2-set clearfix" id="customer_login">

    <div class="col-1">

<?php endif; ?>

    <h2><?php _e( 'Login', 'woocommerce' ); ?></h2>

    <form method="post" class="login">

        <?php do_action( 'woocommerce_login_form_start' ); ?>

        <p class="form-row form-row-wide">
            <input type="text" class="input-text placeholder" placeholder="<?php _e('Username or email', 'woocommerce'); ?>" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
        </p>
        <p class="form-row form-row-wide">
            <input class="input-text placeholder" placeholder="<?php _e('Password', 'woocommerce'); ?>" type="password" name="password" id="password" />
        </p>

        <?php do_action( 'woocommerce_login_form' ); ?>

        <p class="form-row">
            <?php wp_nonce_field( 'woocommerce-login' ); ?>
            <a class="lost_password woo-lost_password2" href="<?php echo esc_url( wc_lostpassword_url() ); ?>"><?php _e( 'Lost Password?', 'woocommerce' ); ?></a>
            <label for="rememberme" class="inline woo-my-account-rememberme">
                <input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php _e( 'Remember me', 'woocommerce' ); ?>
            </label>
        </p>
        <p class="form-row" style="text-align: right;">
            <input type="submit" class="button" name="login" value="<?php _e( 'Login', 'woocommerce' ); ?>" />
        </p>

        <?php do_action( 'woocommerce_login_form_end' ); ?>

    </form>

    <div style="word-break: keep-all;">
        <?php if ('ko' ==  ICL_LANGUAGE_CODE) : ?>
            회원가입은 상품 결제 시 가능합니다.<br>
            (추후 결제화면에서 create an account 에 체크하여 구매정보와 이메일로 계정이 생성됩니다. 회원가입시 주문조회와 리뷰작성이 가능합니다)
        <?php else : ?>
            You can create an account only via Checkout.<br>
            (After you add products on your cart and proceed to checkout, there is a check box to create an account. With your account & email address, you can track order and manage royalty points besides writing review.)
        <?php endif ?>
    </div>


<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

    </div>

    <div class="col-2">

        <h2><?php _e( 'Register', 'woocommerce' ); ?></h2>

        <form method="post" class="register">

            <?php do_action( 'woocommerce_register_form_start' ); ?>

            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

                <p class="form-row form-row-wide">
                    <input type="text" class="input-text placeholder" placeholder="<?php _e('Username', 'woocommerce'); ?>" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
                </p>

            <?php endif; ?>

            <p class="form-row form-row-wide">
                <input type="email" class="input-text placeholder" placeholder="<?php _e('Email', 'woocommerce'); ?>" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />
            </p>

            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

                <p class="form-row form-row-wide">
                    <input type="password" class="input-text placeholder" placeholder="<?php _e('Password', 'woocommerce'); ?>" name="password" id="reg_password" />
                </p>

            <?php endif; ?>

            <!-- Spam Trap -->
            <div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

            <?php do_action( 'woocommerce_register_form' ); ?>
            <?php do_action( 'register_form' ); ?>

            <p class="form-row">
                <?php wp_nonce_field( 'woocommerce-register' ); ?>
                <input type="submit" class="button" name="register" value="<?php _e( 'Register', 'woocommerce' ); ?>" />
            </p>

            <?php do_action( 'woocommerce_register_form_end' ); ?>

        </form>

    </div>

    </div>
<?php endif; ?>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>