<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit; 

global $product, $woocommerce_loop, $eltd_options;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
    $woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
    $woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
    return;

$product_image_src =  wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');

$woo_products_enable_lighbox_icon_yes_no = "yes";
if(isset($eltd_options['woo_products_enable_lighbox_icon'])){
	$woo_products_enable_lighbox_icon_yes_no =  $eltd_options['woo_products_enable_lighbox_icon'];
}

$hide_separator = "no";
if(isset($eltd_options['woo_products_title_separator_hide_title_separator'])){
	$hide_separator = $eltd_options['woo_products_title_separator_hide_title_separator'];
}

$classes = array();
?>

<li <?php post_class( $classes ); ?>>
	<a href="<?php the_permalink(); ?>" class="product_info_overlay"></a>
	<div class="top-product-section">
		<a href="<?php the_permalink(); ?>">
			<span class="image-wrapper">
			<?php
			/**
			 * woocommerce_before_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			do_action( 'woocommerce_before_shop_loop_item_title' );
			?>
			</span>
		</a>
		<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
	</div>
	<div class="product_info_box">
		<span class="product-categories">
		<?php echo wp_kses($product->get_categories(), array(
				'a' => array(
						'href' => true,
						'rel' => true,
						'class' => true,
						'title' => true,
						'id' => true
				)
				
		));
		?>
		</span>
		<a href="<?php the_permalink(); ?>">
			<span class="product-title"><?php the_title(); ?></span>
		</a>
		<?php if($hide_separator == "no") { ?>
		<div class="separator_holder">
			<span class="separator medium"></span>
		</div>
		<?php } ?>
		<div class="shop_price_lightbox_holder">
		<?php
		/**
		 * woocommerce_after_shop_loop_item_title hook
		 *
		 * @hooked woocommerce_template_loop_rating - 5
		 * @hooked woocommerce_template_loop_price - 10
		 */
		do_action( 'woocommerce_after_shop_loop_item_title' );
		?>
		</div>
	</div>
</li>
