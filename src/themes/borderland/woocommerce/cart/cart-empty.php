<?php
/**
 * Empty cart page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices();

?>

<div class="empty-cart-wrapper">
	
	<?php do_action('woocommerce_cart_is_empty'); ?>
	<p class="return-to-shop">
		<a class="button tiny" href="<?php echo esc_url( home_url( '/' ) ); ?>">
			<?php _e( 'Return To Home', 'woocommerce' ) ?>
		</a>
	</p>
	
</div>