<?php
add_action( 'after_setup_theme', 'blankslate_theme_setup' );
function blankslate_theme_setup() {
	//add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	//add_theme_support( 'wc-product-gallery-slider' );
	
	register_nav_menus( array(
			'header' => 'Header menu',
			'footer' => 'Footer menu'
	) );
}

add_action( 'wp_enqueue_scripts', 'blankslate_load_scripts' );
function blankslate_load_scripts() {
	wp_enqueue_script( 'jquery' );
}

function my_comment($comment, $args, $depth) {
    ?>
    <li>
        <div class="comment clearfix">
            <div class="image">
                <?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment ); ?>
            </div>

            <div class="text">
                <div class="comment_info">
                    <h6 class="name"><?php printf( __( '%s' ), get_comment_author_link() ); ?></h6>
                    <?php comment_reply_link( array_merge( $args, array( 'add_below' => '', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
                    <?php edit_comment_link( __( 'Edit This' ), '  ', '' ); ?>
                </div>
                <div class="text-holder" id="comment-<?php comment_ID() ?>">
                    <?php comment_text(); ?>
                </div>
                <span class="comment-date"><?php printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time() ); ?></span>
            </div>
        </div>
    <?php
    // li tag will be closed by WordPress after looping through child elements
}

// Display 24 products per page. Goes in functions.php
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 8;' ), 20 );

// product tabs
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
function woo_remove_product_tabs( $tabs ) {
	//unset( $tabs['description'] );      	// Remove the description tab
	unset( $tabs['additional_information'] );  	// Remove the additional information tab
	return $tabs;
}

/* redirect product detail page, (add_to_cart) */
function woocommerce_template_single_add_to_cart() {
    global $product;

    $product_id = $product->id;
    $product_type = get_post_meta( $product_id, 'product-type', true );
    if ('redirect' == $product_type) {
        $redirect_url = get_post_meta( $product_id, 'redirect-url', true );
        echo '<a href=//'.$redirect_url.' target="_blank" class="cart" style="display: table;">'.
                '<button type="submit" class="single_add_to_cart_button qbutton button alt" style="margin-left:0 !important;" >Redirect</button>'.
             '</a>';
    } else {
        do_action( 'woocommerce_' . $product->product_type . '_add_to_cart'  );
    }
}

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );

//////
add_filter('logout_url', 'wpse_44020_logout_redirect', 10, 2);
function wpse_44020_logout_redirect($logouturl, $redir) {
    return $logouturl . '&amp;redirect_to='.get_permalink();
}

// ////


// add to cart 버튼
add_filter( 'woocommerce_loop_add_to_cart_link', 'august_redirect_cart', 98 );
function august_redirect_cart($link) {
	global $product;
	$product_type = get_post_meta($product->id, 'product-type', true);
	$redirect_url = get_post_meta($product->id, 'redirect-url', true);
	
	$add_to_cart_url = $product->add_to_cart_url();
	$target = '_self';
	$add_to_cart_text = $product->add_to_cart_text();
	if ($product_type == 'redirect-url') {
		$add_to_cart_url = $redirect_url;
		$target = '_blank';
		$add_to_cart_text = 'Redirect';
	}
	
	return sprintf( '<div class="product_image_overlay"></div>'.
					'<div class="add-to-cart-button-outer">'.
    					'<div class="add-to-cart-button-inner">'.
    						'<div class="add-to-cart-button-inner2">'.
    							'<a class="product_link_over" href="%s" target="_blank"></a>'.
    							'<a href="%s" target="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class="qbutton add-to-cart-button button">%s</a>'.
    						'</div>'.
    					'</div>'.
    				'</div>',
    	esc_url( get_the_permalink() ),
    	esc_url( $add_to_cart_url),
    	esc_attr( $target ),
    	esc_attr( $product->get_id() ),
    	esc_attr( $product->get_sku() ),
    	esc_attr( isset( $quantity ) ? $quantity : 1 ),
    	esc_html( $add_to_cart_text )
	);
	
}

// remove short description
add_filter( 'woocommerce_short_description', 'august_short_description', 98);
function august_short_description($variable) {
	return '';
}

// product long body
add_action( 'my_after_single_product_summary', 'august_after_single_product_summary', 20);
function august_after_single_product_summary() {
	global $product;
	$desc = do_shortcode($product->description);
	echo '<div class="kt_product-desctiption">'.$desc.'</div>';
}

// add to cart message
add_filter( 'wc_add_to_cart_message_html', 'august_add_to_cart_message_html' );
function august_add_to_cart_message_html() {
	return null;
}

// post, excerpt
function august_excerpt_more( $more ) {
	return '...';
}
add_filter( 'excerpt_more', 'august_excerpt_more' );
