<?php global $wp_query; if ( $wp_query->max_num_pages > 1 ) { ?>
    <nav id="nav-below" class="navigation" role="navigation">
        <div class="nav-previous">
            <?php /* previous_posts_link(sprintf( __( 'newer %s', 'blankslate' ), '<span class="meta-nav">&rarr;</span>' ) ) */ ?>
            <?php previous_posts_link(sprintf('<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>')) ?>
        </div>
        <div class="nav-next">
            <?php /* next_posts_link(sprintf( __( '%s older', 'blankslate' ), '<span class="meta-nav">&larr;</span>' ) ) */ ?>
            <?php next_posts_link(sprintf('<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>')) ?>
        </div>
    </nav>
<?php } ?>