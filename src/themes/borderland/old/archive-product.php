<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
<script src="<?php echo get_stylesheet_directory_uri();?>/3rdparty/lory.min.js"></script>
<style>
    div#container-inner {
        width: 100%;
        padding: 0;
    }
    div#container { background-color: #222 !important; }

    .slider {
        position: relative;
        width: 100%;
        -webkit-user-select: none;
        -moz-user-select: -moz-none;
        user-select: none;
        margin-bottom: 2%;
        overflow: hidden;
    }

    .frame,
    .events_log {
        position: relative;
        margin: 0 auto;
        font-size: 0;
        line-height: 0;
        overflow: visible;
        white-space: nowrap;
    }

    .frame li {
        position: relative;
        display: inline-block;
        width: 100%; height: 130px;
        text-align: center;
        font-size: 15px;
        line-height: 30px;
        color: #fff;
        overflow: hidden;
    }

    .slides { display: inline-block; }
    .percentage .slides { display: block; }
    .percentage .frame { width: 100%; }
    .percentage li { width: 100%; }
    .variablewidth li {
        width: 280px;
        margin-right: 10px;
    }
    .multipleelements li,
    .multislides li,
    .ease li {
        width: 60px;
        margin-right: 10px;
    }
    .simple li:last-child,
    .rewind li:last-child,
    .events li:last-child,
    .variablewidth li:last-child,
    .multipleelements li:last-child,
    .multislides li:last-child,
    .ease li:last-child { margin-right: 0; }
    .prev, .next {
        position: absolute;
        top: 50%;
        margin-top: -25px;
        display: block;
        cursor: pointer;
    }
    .next { right: 0; }
    .prev { left: 0; }
    .next svg, .prev svg { width: 25px; }

    .woocommerce .products ul,
    .woocommerce ul.products {
        margin-bottom: 0 !important;
    }

    /* 임시 컬러 */
    .slider .slide-glass {
        position: absolute;
        width: 100%; height: 100%;
        left: 0; top: 0;
    }
    .slider .slide-image {
        width: 100%; height: 100%;
        background-position: center center;
        background-size: cover;
    }
    .slider .slide-description {
        position: absolute;
        padding: 5px 10px;
        left: 0;
        bottom: 0;
        background-color: rgba(0,0,0,0.6);
    }

    .product-post-outer {
        position: relative;
        width: 100%; height: 270px;
        margin-bottom: 2%;
    }

    .product-post-outer .post-image {
        position: absolute;
        width: 100%; height: 100%;
        left: 0; top: 0;
        background-position: center center;
        background-size: cover;
    }

    .product-post-outer .post-title {
        position: absolute;
        width: 100%;
        bottom: 0;
        padding: 40px 20px 20px;
        font-size: 28px;
        color: #fff;
        font-weight: 700;
        letter-spacing: 3px;
        background: -webkit-linear-gradient(rgba(0,0,0,0), rgba(0,0,0,0.6)); /* For Safari 5.1 to 6.0 */
        background: -o-linear-gradient(rgba(0,0,0,0), rgba(0,0,0,0.6)); /* For Opera 11.1 to 12.0 */
        background: -moz-linear-gradient(rgba(0,0,0,0), rgba(0,0,0,0.6)); /* For Firefox 3.6 to 15 */
        background: linear-gradient(rgba(0,0,0,0), rgba(0,0,0,0.6)); /* Standard syntax */
    }
</style>

<?php
    /**
     * woocommerce_before_main_content hook.
     *
     * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
     * @hooked woocommerce_breadcrumb - 20
     */
    do_action( 'woocommerce_before_main_content' );
?>

    <?php if ( apply_filters( 'woocommerce_show_page_title', false ) ) : ?>

        <h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

    <?php endif; ?>

    <?php if ( have_posts() ) : ?>

        <div class="slider js_simple simple">
            <div class="frame js_frame">
                <ul class="slides js_slides" style="height: 500px;">
                    <li class="js_slide" id="about-kew">
                        <div class="slide-glass"></div>
                        <div class="slide-image" style="background-image: url(https://sphouse.co/wp-content/uploads/sites/2/2016/04/spirit_slide3.1603.jpg)"></div>
                        <div class="slide-description">1</div>
                    </li>
                    <li class="js_slide" id="digital-art">
                        <div class="slide-glass"></div>
                        <div class="slide-image" style="background-image: url(https://sphouse.co/wp-content/uploads/sites/2/2016/04/spirit_slide4.1603.jpg)"></div>
                        <div class="slide-description">2</div>
                    </li>
                    <li class="js_slide" id="about-spirithouse">
                        <div class="slide-glass"></div>
                        <div class="slide-image" style="background-image: url(https://sphouse.co/wp-content/uploads/sites/2/2016/04/spirit_eng.1603.jpg)"></div>
                        <div class="slide-description">3</div>
                    </li>
                    <li class="js_slide" id="about-journal">
                        <div class="slide-glass"></div>
                        <div class="slide-image" style="background-image: url(https://sphouse.co/wp-content/uploads/sites/2/2016/04/spirit_kor.1603.jpg)"></div>
                        <div class="slide-description">4</div>
                    </li>
                </ul>
            </div>
            <span class="js_prev prev">
                <svg xmlns="https://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 501.5 501.5">
                    <g>
                        <path fill="#ECF0F1" d="M302.67 90.877l55.77 55.508L254.575 250.75 358.44 355.116l-55.77 55.506L143.56 250.75z"></path>
                    </g>
                </svg>
            </span>
            <span class="js_next next">
                <svg xmlns="https://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 501.5 501.5">
                    <g>
                        <path fill="#ECF0F1" d="M199.33 410.622l-55.77-55.508L247.425 250.75 143.56 146.384l55.77-55.507L358.44 250.75z"></path>
                    </g>
                </svg>
            </span>
        </div>


        <?php woocommerce_product_loop_start(); ?>

            <?php woocommerce_product_subcategories(); ?>

            <?php while ( have_posts() ) : the_post(); ?>

                <?php wc_get_template_part( 'content', 'product' ); ?>

            <?php endwhile; // end of the loop. ?>

        <?php woocommerce_product_loop_end(); ?>

        <?php
            /**
             * woocommerce_after_shop_loop hook.
             *
             * @hooked woocommerce_pagination - 10
             */
            do_action( 'woocommerce_after_shop_loop' );
        ?>

        <!-- Blog Contents --->
        <div class="product-post-outer">
            <div class="post-image" style="background-image: url(https://sphouse.co/wp-content/uploads/sites/2/2015/03/bg-history-of-brooklyn.jpg)"></div>
            <div class="post-title">HISTORY OF BROOKLYN</div>
        </div>

        <div class="product-post-outer">
            <div class="post-image" style="background-image: url(https://sphouse.co/wp-content/uploads/sites/2/2015/03/bg-morocco-road-trip.jpg)"></div>
            <div class="post-title">MOROCCO ROAD TRIP</div>
        </div>


    <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

        <?php wc_get_template( 'loop/no-products-found.php' ); ?>

    <?php endif; ?>

<?php
    /**
     * woocommerce_after_main_content hook.
     *
     * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
     */
    do_action( 'woocommerce_after_main_content' );
?>

<?php
    /**
     * woocommerce_sidebar hook.
     *
     * @hooked woocommerce_get_sidebar - 10
     */
    do_action( 'woocommerce_sidebar' );
?>

<?php get_footer( 'shop' ); ?>

<script type="text/javascript">
	'use strict';
	$ = jQuery;

	var myLory = {};
	var dummyWidth = $("#container").width();
    var dummyHeight = $("#container").height();

    // document loaded
    document.addEventListener('DOMContentLoaded', function () {
        initLory();
    });

    // resize event
    $(window).on('load', function () {
        $(this).trigger('resize');
    });

    $(window).resize(function() {
        dummyWidth = $("#container").width();
        dummyHeight = $("#container").height();
        resizeLory();
    });

    function initLory() {
        var simple = document.querySelector('.js_simple');

        $(".js_simple").width(dummyWidth);
        $(".js_simple .frame").width(dummyWidth);
        $(".js_simple li.js_slide").width(dummyWidth);

        var slideNumber = $(simple).find("li.js_slide").length;

        // setup lory
        myLory = lory(simple, {
            infinite: 1,
            enableMouseEvents: true,
            slideSpeed: 450
        });
        myLory.slideNumber = slideNumber;

        $(".js_simple li.js_slide").height(500);

        // set navigation (custom)
        // $(".slide-navigation").loryNavi(myLory, simple);

        // $($("#about-kew .slide-image")[0]).asyncBackgroundImage("#666");
    }

    function resizeLory() {
        $(".js_simple").width(dummyWidth);
        $(".js_simple .frame").width(dummyWidth);
        $(".js_simple li.js_slide").width(dummyWidth);
    }
</script>
