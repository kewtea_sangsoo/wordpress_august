/**
 * Share.js
 *
 * @author  overtrue <i@overtrue.me>
 * @license MIT
 *
 * @example
 * <pre>
 * $('.share-components').share();
 *
 * // or
 *
 * $('.share-bar').share({
 *     sites: ['qzone', 'qq', 'weibo','wechat'],
 *     // ...
 * });
 * </pre>
 */
;(function($){
    /**
     * Initialize a share bar.
     *
     * @param {Object}        $options globals (optional).
     *
     * @return {void}
     */
    $.fn.share = function ($options) {
        var $head = $(document.head);

        var $defaults = {
            url: location.href,
            site_url: location.origin,
            source: $head.find('[name=site], [name=Site]').attr('content') || document.title,
            title: $head.find('[name=title], [name=Title]').attr('content') || document.title,
            description: $head.find('[name=description], [name=Description]').attr('content') || '',
            image: $('img:first').prop('src') || '',
            imageSelector: undefined,

            weiboKey: '',

            wechatQrcodeHelper: 'share by QR code image',
            wechatQrcodeSize: 100,

            mobileSites: [],
            sites: ['weibo','wechat','line','facebook','twitter', 'email'],
            disabled: [],
            initialized: false
        };

        var $globals = $.extend({}, $defaults, $options);

        var $templates = {
            weibo       : 'http://service.weibo.com/share/share.php?url={{URL}}&title={{TITLE}}&pic={{IMAGE}}&appkey={{WEIBOKEY}}',
            wechat      : '', //'javascript:;',
            facebook    : 'https://www.facebook.com/sharer/sharer.php?u={{URL}}',
            twitter     : 'https://twitter.com/intent/tweet?text={{TITLE}}&url={{URL}}',
            line        : 'javascript:;',
            email       : '{{URL}}'
        };

        var $prettyNames = {
            weibo       : 'Weibo',
            wechat      : 'WeChat',
            facebook    : 'Facebook',
            twitter     : 'Twitter',
            line        : 'Line',
            email       : 'via email'
        };

        this.each(function() {
            if ($(this).data('initialized')) {
                return true;
            }

            var $data      = $.extend({}, $globals, $(this).data());
            if ($data.imageSelector) {
                $data.image = $($data.imageSelector).map(function() {
                    return $(this).prop('src');
                }).get().join('||');
            }
            var $container = $(this).addClass('share-component social-share');

            createIcons($container, $data);
            createWechat($container, $data);
            createLine($container, $data);

            $(this).data('initialized', true);
        });

        /**
         * Create site icons
         *
         * @param {Object|String} $container
         * @param {Object}        $data
         */
        function createIcons ($container, $data) {
            var $sites = getSites($data);

            $data.mode == 'prepend' ? $sites.reverse() : $sites;

            if (!$sites.length) {return;}

            $.each($sites, function (i, $name) {
                var $url  = makeUrl($name, $data);
                var $link = $data.initialized ? $container.find('.icon-'+$name) : $('<a class="social-share-icon icon-'+$name+'"></a>');

                if (!$link.length) {
                    return true;
                }

                $link.prop('href', $url);

                if ($name === 'wechat') {
                    $link.prop('tabindex', -1);
                } else if ($name === 'email') {
                    $link.prop('href', "mailto:?subject="+$data.title+"&amp;&body="+$url);
                    $link.append('<span class="glyphicon glyphicon-envelope"></span>');
                } else {
                    $link.prop('target', '_blank');
                }

                var $wrapper = $('<div class="social-share-wrapper"></div>');
                $wrapper.append($link);

                // 설명 붙이기
                var $desc = '<div class="share-icon-desc">'+$prettyNames[$name]+'</div>';
                $wrapper.append($desc);

                if (!$data.initialized) {
                    $data.mode == 'prepend' ? $container.prepend($wrapper) : $container.append($wrapper);
                }
            });
        }

        /**
         * Create the wechat icon and QRCode.
         *
         * @param {Object|String} $container
         * @param {Object}        $data
         */
        function createWechat ($container, $data) {
            var $wechat = $container.find('a.icon-wechat');

            if (!$wechat.length) {return;}

            $wechat.append('<div class="wechat-qrcode"><div class="qrcode"></div><div class="help">'+$data.wechatQrcodeHelper+'</div></div>');

            $wechat.find('.qrcode').qrcode({
                render: 'image',
                size: $data.wechatQrcodeSize,
                text: $data.url,
                background: "#ffffff"
            });

            $wechat.removeAttr("href");

            $wechat.click(function() {
                $wechat.toggleClass("clicked");
            });
            $(window).click(function(event) {
                if (0 == $(event.target).parents('.social-share.share-component').length)
                    $wechat.removeClass("clicked");
            });

            if ($wechat.offset().top < 100) {
                $wechat.find('.wechat-qrcode').addClass('bottom');
            }
        }

        /**
         * Create the line icon.
         *
         * @param {Object|String} $container
         * @param {Object}        $data
         */
        function createLine ($container, $data) {
            var $line = $container.find('a.icon-line');

            if (!$line.length) {return;}

            $line.append('<span>' +
                            '<script type="text/javascript">' +
                                'new media_line_me.LineButton({"pc":false,"lang":"en","type":"d","text":"'+encodeURIComponent($data.title)+'","withUrl":true});' +
                            '</script>' +
                        '</span>');
        }

        /**
         * Get available site lists.
         *
         * @param {Array} $data
         *
         * @return {Array}
         */
        function getSites ($data) {
            if ($data['mobileSites'].length === 0 && $data['sites'].length) {
                $data['mobileSites'] = $data['sites'];
            }

            var $sites = (isMobileScreen() ? $data['mobileSites'] : ($data['sites'].length ? $data['sites']: [])).slice(0);
            var $disabled = $data['disabled'];

            if (typeof $sites == 'string') { $sites = $sites.split(/\s*,\s*/); }
            if (typeof $disabled == 'string') { $disabled = $disabled.split(/\s*,\s*/); }

            if (runningInWeChat()) {
                $disabled.push('wechat');
            }
            // Remove elements
            $disabled.length && $.each($disabled, function (i, el) {
                var removeItemIndex = $.inArray(el, $sites);
                if (removeItemIndex !== -1) {
                    $sites.splice(removeItemIndex, 1);
                }
            });

            return $sites;
        }

        /**
         * Build the url of icon.
         *
         * @param {String} $name
         * @param {Object} $data
         *
         * @return {String}
         */
        function makeUrl ($name, $data) {
            var $template = $templates[$name];
            $data['summary'] = $data['description'];

            for (var $key in $data) {
                if ($data.hasOwnProperty($key)) {
                    var $camelCaseKey = $name + $key.replace(/^[a-z]/, function($str){
                        return $str.toUpperCase();
                    });

                    var $value = encodeURIComponent($data[$camelCaseKey] === undefined ? $data[$key] : $data[$camelCaseKey]);
                    $template = $template.replace(new RegExp('{{'+$key.toUpperCase()+'}}', 'g'), $value);
                }
            }

            return $template;
        }

        /**
         * Detect wechat browser.
         *
         * @return {Boolean}
         */
        function runningInWeChat() {
            return /MicroMessenger/i.test(navigator.userAgent);
        }

        /**
         * Mobile screen width.
         *
         * @return {boolean}
         */
        function isMobileScreen () {
            return $(window).width() <= 768;
        }
    };

    // Domready after initialization
    $(function () {
        $('.share-component,.social-share').share();
    });
})(jQuery);
