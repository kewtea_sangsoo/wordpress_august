/**
 * 파라미터로 받은 url 을 공유하는 팝업 UI 생성 (bootstrap modal)
 *
 * @param url
 * @constructor
 */
function ShareUI(url) {

    this.url = url || window.location.href; // 공유 하려는 url 주소 (detail page url)

    this.$html = $('<div class="modal fade" id="share-modal" tabindex="-1" role="dialog">'+
                    '<div class="modal-dialog" role="document">'+
                        '<div class="modal-content">'+
                            '<div class="modal-header">'+
                                '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                                '<h4 class="modal-title">Share</h4>'+
                            '</div>'+
                            '<div class="modal-body">'+
                                '<div class="social-share"></div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>');
}

/**
 * jquery share 라이브러리 로딩하여 팝업창 띄움
 *
 * @param config
 */
ShareUI.prototype.readyPopup = function(config) {
    $('body').append(this.$html);

    // we-chat 열려있을 땐, we-chat 만 닫기게 하기
    $('#share-modal').on('hide.bs.modal', function () {
        if ($(".social-share-icon.icon-wechat").hasClass("clicked")) return false;
    });

    $('.social-share').share(config);

    // line 찾아서 display: none 동기화
    var icons = $('.social-share a.social-share-icon');
    for (var i=0; i<icons.length; i++) {
        var $icon = $(icons[i]);
        var $wrapper = $icon.parent();
        if ($icon.css('display') == 'none') $wrapper.css('display', 'none');
    }
};