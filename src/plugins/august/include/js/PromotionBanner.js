/**
 * 화면하단에 나오는 광고배너
 *
 * @constructor
 */
function PromotionBanner() {
    var that = this;
    this.$html = $('<div class="promotion-banner promotion-hide">'+
                        '<span class="close-btn glyphicon glyphicon-remove"></span>'+
                        '<a class="promotion-anchor" target="_blank">'+
                            '<div class="promotion-banner-content">'+
                                '<div class="promotion-title"></div>'+
                                '<div class="promotion-content"></div>'+
                            '</div>'+
                        '</a>'+
                    '</div>');

    this.$html.find(".close-btn").click(function(event) {
        event.stopPropagation();
        that.hideAnimation();
    });

    this.onScroll = function() {
        if (30 < window.scrollY) that.hide();
        else that.show();
    };

    this.showAnimation = function() {
        $(window).off("scroll", this.onScroll);
        var $banner = this.$html;
        $banner.removeClass("promotion-hide");
        $banner.animate({
            bottom: 0
        }, 300, function() {
            $(window).on("scroll", that.onScroll);
        });
    };

    this.hideAnimation = function() {
        $(window).off("scroll", this.onScroll);
        var $banner = this.$html;
        var height = $banner.height();
        $banner.animate({
            bottom: -height
        }, 300, function() {
            $banner.addClass("promotion-hide");
            // $(window).on("scroll", that.onScroll);
        });
    };
}

/**
 * 배너안에 넣을 컨텐츠 html 코드로 넣기 (이 부분 얘기나온게 없어서 이정도로만 구현)
 *
 * @param {Object} data
 * @return {void}
 */
PromotionBanner.prototype.setContent = function(data) {

    // set url anchor
    if (data.url) this.$html.find(".promotion-anchor").attr("href", data.url);

    // set title
    if (data.title) this.$html.find(".promotion-title").text(data.title);
    else this.$html.find(".promotion-title").css("display", "none");

    // set content (string or <img>)
    if (data.content) this.$html.find(".promotion-content").html(data.content);

    // set style
    if (data.style) {
        if (data.style.bgColor) this.setBg(data.style.bgColor);
        if (data.style.titleColor) this.$html.find(".promotion-title").css("color", data.style.titleColor);
        if (data.style.contentColor) this.$html.find(".promotion-content").css("color", data.style.contentColor);
    }

};

/**
 * 배너 배경색깔 지정
 *
 * @param {String} colorCode | ex) "#333333"
 * @return {void}
 */
PromotionBanner.prototype.setBg = function(colorCode) {
    if ("string" == typeof colorCode) this.$html.css("background-color", colorCode);
    else console.warn("colorCode type error: {"+typeof colorCode+"} colorCode="+colorCode);
};

/**
 * 없으면 붙이고, show .. scroll event 동작시켜서 스크롤 내려가면 가려준다
 *
 * @return {void}
 */
PromotionBanner.prototype.show = function() {
    var $banner = this.$html;
    if (0 == $('body').find($banner).length) $('body').append($banner);
    if ($banner.hasClass("promotion-hide")) this.showAnimation();
};

PromotionBanner.prototype.hide = function() {
    var $banner = this.$html;
    if (!$banner.hasClass("promotion-hide")) this.hideAnimation();
};

PromotionBanner.prototype.remove = function() {
    $(window).off("scroll", this.onScroll);
    this.$html.remove();
};