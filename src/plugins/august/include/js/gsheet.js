console.log("gsheet.js loaded");
function GoogleSheetService(sheetId) {
	this.sheetId = sheetId;
	
    this.get = function(sheetNumber) {
    	var sheetURL = "https://spreadsheets.google.com/feeds/list/"+
    					this.sheetId+"/"+
    					sheetNumber+"/public/values?alt=json";
    	
        return new Promise(function(resolve, reject) {
        	$.getJSON(sheetURL, {
                format: "json"
            }).done(function(receivedData) {
                try {
                    var data = [];
                    var rows = receivedData.feed.entry;
                    for (var i=0; i<rows.length; i++) {
                        data[i] = {};

                        var row = rows[i];
                        var rowNames = Object.getOwnPropertyNames(row);
                        var filtered = rowNames.filter(function(value) {
                            return value.indexOf("gsx$") > -1;
                        });

                        for (var j=0; j<filtered.length; j++) {
                            var column = filtered[j].substr(4);
                            data[i][column] = row[filtered[j]].$t;
                        }
                    }
                    resolve(data);
                } catch(error) {
                    console.warn(model);
                    console.warn(error);
                }
            }).fail(function(error) {
            	console.warn(model);
                console.warn(error);
            });
        });    	
    }
}