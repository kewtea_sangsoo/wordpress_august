;console.log("get-promotions.js loaded");
$ = $ || jQuery;
var service = new GoogleSheetService(sheetId);
service.get(sheetNum).then(function(response) {
	if (0 < response.length) {
		for (var i=0; i<response.length; i++) {
			var promotion= response[i];
			if (pagePath == promotion['target-path']) {
				showPromotion(promotion);
			}
		}
	}
});

function showPromotion(promotion) {
	var title = promotion['title'];
	var titleColor = promotion['title-color'];
	var content = promotion['content'];
	var link = promotion['link'];
	var contentColor = promotion['content-color'];
	var bgColor = promotion['bg-color'];
	
	var banner = new PromotionBanner();
	var data = {
            url: link,
            title: title,
            content: content,
            style: {
                bgColor: bgColor,
                titleColor: titleColor,
                contentColor: contentColor
            }
        };
    banner.setContent(data);
    banner.show();
}