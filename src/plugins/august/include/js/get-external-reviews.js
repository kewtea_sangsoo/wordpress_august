;console.log("get-external-reviews.js loaded");
$ = $ || jQuery;
var service = new GoogleSheetService(sheetId);
service.get(sheetNum).then(function(response) {
	if (0 < response.length) {
		$(".woocommerce-Reviews-title").after('<ol id="external-reviews" class="commentlist"></ol>');
		for (var i=0; i<response.length; i++) {
			var review = response[i];
			if (productId == review['product-id']) {
				$("#external-reviews").append(newReview(review));
			}
		}
	}
});

function newReview(review) {
	var authorName = review['author-name'];
	var comment = review['comment'];
	var createdTime = review['created-time'];
	var rate = review['rate'];
	var rateW = rate*20;
	var elem =
		'<li class="comment even thread-even depth-1">'+
			'<div class="comment_container">'+
				//<img alt="" src="http://2.gravatar.com/avatar/22d682df186617ae2a91259b3fd5015f?s=60&amp;d=mm&amp;r=g" srcset="http://2.gravatar.com/avatar/22d682df186617ae2a91259b3fd5015f?s=120&amp;d=mm&amp;r=g 2x" class="avatar avatar-60 photo" height="60" width="60">
				'<div class="comment-text">'+
					'<div class="star-rating">'+
						'<span style="width:'+rateW+'%"><strong>'+rate+'</strong> out of 5</span>'+
					'</div>'+
					'<p class="meta">'+
						'<strong class="woocommerce-review__author" itemprop="author">'+
							authorName+
						'</strong>'+
						'<span class="woocommerce-review__dash">–</span>'+
						'<time class="woocommerce-review__published-date" itemprop="datePublished">'+
							createdTime+
						'</time>'+
					'</p>'+
					'<div class="description">'+
						'<p>'+comment+'</p>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</li>';
	return elem;
}