;console.log('share-product.js loaded');
var config = {
    url: location.href,
    title: title,
    image: mainImageURL
};

var sharePopup = new ShareUI();
sharePopup.readyPopup(config);

$(".product_meta").append(
	'<div class="detail-btn share-btn" data-toggle="modal" data-target="#share-modal">'+
	    '<span class="share-text">Share</span>'+
	    '<span class="glyphicon glyphicon-share" aria-hidden="true"></span>'+
    '</div>'
);