<?php
class August_Setting_View {
	public function __construct(){ }
	
	public function setup_sections_fields() {
		
		// 1) Visual
		if( false == get_option( 'aug_visual_options' ) ) {
			add_option( 'aug_visual_options' );
		} // end if
		
		add_settings_section(
			'aug_visual_section', // ID used to identify this section and with which to register options
			'Visual Options',     // Title to be displayed on the administration page
			false,                // Callback used to render the description of the section
			'aug_visual_options'  // Page on which to add this section of options
		);
		
		// product-redirect
		add_settings_field(
			'product_redirect',
			'Redirect',
			array( 'August_Setting_View', 'aug_redirect_callback' ),
			'aug_visual_options',
			'aug_visual_section'
		);
		register_setting(
			'aug_visual_options',
			'product_redirect'
		);
		
		
		// product-review
		add_settings_field(
			'product_reviews',
			'Product Reviews',
			array( 'August_Setting_View', 'aug_review_callback' ),
			'aug_visual_options',
			'aug_visual_section'
		);
		register_setting(
			'aug_visual_options',
			'product_reviews'
		);
		
		// promotion
		add_settings_field(
			'promotion',
			'Promotion',
			array( 'August_Setting_View', 'aug_promotion_callback' ),
			'aug_visual_options',
			'aug_visual_section'
		);
		register_setting(
			'aug_visual_options',
			'promotion'
		);
		
		// product-share
		add_settings_field(
			'product_share',
			'Product Share',
			array( 'August_Setting_View', 'aug_product_share_callback' ),
			'aug_visual_options',
			'aug_visual_section'
		);
		register_setting(
			'aug_visual_options',
			'product_share'
		);
		
		// shortcode ([vbox ...])
		add_settings_field(
			'shortcode',
			'Shortcode',
			array( 'August_Setting_View', 'aug_shortcode_callback' ),
			'aug_visual_options',
			'aug_visual_section'
		);
		register_setting(
			'aug_visual_options',
			'shortcode'
		);
		
		// 일단 숨기기 ... default enable 이라고 생각하고 .. (추후고급유저용)
		// js enable/disable (default enable)
		/*
		add_settings_field(
			'js',
			'Javascript',
			array( 'August_Setting_View', 'aug_js_callback' ),
			'aug_visual_options',
			'aug_visual_section'
		);
		register_setting(
			'aug_visual_options',
			'js'
		);
		*/
		
		// option values grouping
		register_setting(
			'aug_visual_options',
			'aug_visual_options'
		);
		
		
		// 2) DB
		if( false == get_option( 'aug_db_options' ) ) {
			add_option( 'aug_db_options' );
		} // end if
		
		add_settings_section(
			'aug_db_section',
			'DB Options',
			false,
			'aug_db_options'
		);
		
		// enable sync with external db
		add_settings_field(
			'sync_external',
			'Sync with external DB',
			array( 'August_Setting_View', 'aug_db_sync_callback' ),
			'aug_db_options',
			'aug_db_section'
		);
		register_setting(
			'aug_db_options',
			'sync_external'
		);
		
		// rest api pass phrase
		add_settings_field(
			'pass_phrase',
			'Rest API Pass phrase',
			array( 'August_Setting_View', 'aug_pass_phrase_callback' ),
			'aug_db_options',
			'aug_db_section'
		);
		register_setting(
			'aug_db_options',
			'pass_phrase'
		);
		
		// option values grouping
		register_setting(
			'aug_db_options',
			'aug_db_options'
		);
		
		// 3) SEO
		// ...
		
		
		// Version is ...
		
		
	}
	
	
	// visual/redirect view
	function aug_redirect_callback() {
		$options = get_option('aug_visual_options');
		
		echo '<input id="product_redirect" name="aug_visual_options[product_redirect]" '.
					 'type="checkbox" value="1" '.
					 checked(1, $options['product_redirect'], false).'/>';
		echo '<label for="product_redirect">Enable Redirect</label>';
		
		// log
		echo '<br>$options[\'product_redirect\']='.$options['product_redirect'];
	}
	
	// visual/review view
	function aug_review_callback() {
		$options = get_option('aug_visual_options');
		
		echo '<select id="product_reviews" name="aug_visual_options[product_reviews]">';
			echo '<option value="hide" '.selected($options['product_reviews'], 'hide').'>Hide reviews</option>';
			echo '<option value="gsheet" '.selected($options['product_reviews'], 'gsheet').'>Overlay by Google Sheet</option>';
			echo '<option value="wordpress" '.selected($options['product_reviews'], 'wordpress').'>Use wordpress original</option>';
		echo '</select>';
		
		echo '<p class="description">if you select google sheet, you need to type google sheet info.</p>';
		
		echo 'Google sheet Id:';
		echo '<input type="text" id="review_gsheet_id"'.
					'name="aug_visual_options[review_gsheet_id]"'.
					'value="'.$options['review_gsheet_id'].'" />';
		echo '<br>';
		echo 'Sheet number:';
		echo '<input type="text" id="review_gsheet_num"'.
					'name="aug_visual_options[review_gsheet_num]"'.
					'value="'.$options['review_gsheet_num'].'" />';
		
		// log
		echo '<br>$options[\'product_reviews\']='.$options['product_reviews'];
		echo '<br>$options[\'review_gsheet_id\']='.$options['review_gsheet_id'];
		echo '<br>$options[\'review_gsheet_num\']='.$options['review_gsheet_num'];
	}
	
	// visual/promotion view
	function aug_promotion_callback() {
		$options = get_option('aug_visual_options');
		
		echo '<input id="promotion" name="aug_visual_options[promotion]" '.
					'type="checkbox" value="1" '.
					checked(1, $options['promotion'], false).'/>';
		echo '<label for="promotion">Use Promotion</label>';
		
		echo '<br><br>';
		
		echo 'Google sheet Id:';
		echo '<input type="text" id="promotion_gsheet_id"'.
				'name="aug_visual_options[promotion_gsheet_id]"'.
				'value="'.$options['promotion_gsheet_id'].'" />';
		echo '<br>';
		echo 'Sheet number:';
		echo '<input type="text" id="promotion_gsheet_num"'.
				'name="aug_visual_options[promotion_gsheet_num]"'.
				'value="'.$options['promotion_gsheet_num'].'" />';
		
		// log
		echo '<br>$options[\'promotion\']='.$options['promotion'];
		echo '<br>$options[\'promotion_gsheet_id\']='.$options['promotion_gsheet_id'];
		echo '<br>$options[\'promotion_gsheet_num\']='.$options['promotion_gsheet_num'];
	}
	
	// visual/product-share view
	function aug_product_share_callback() {
		$options = get_option('aug_visual_options');
		
		echo '<input id="product_share" name="aug_visual_options[product_share]" '.
					'type="checkbox" value="1" '.
					checked(1, $options['product_share'], false).'/>';
		echo '<label for="product_share">Use Product Share</label>';
		
		// log
		echo '<br>$options[\'product_share\']='.$options['product_share'];
	}
	
	// visual/shortcode view
	function aug_shortcode_callback() {
		$options = get_option('aug_visual_options');
		
		echo '<input id="shortcode" name="aug_visual_options[shortcode]" '.
					'type="checkbox" value="1" '.
					checked(1, $options['shortcode'], false).'/>';
		echo '<label for="shortcode">Use Shortcode</label>';
		
		// log
		echo '<br>$options[\'shortcode\']='.$options['shortcode'];
	}
	
	// visual/js_enable view
	function aug_js_callback() {
		$options = get_option('aug_visual_options');
		
		echo '<input id="js" name="aug_visual_options[js]" '.
					'type="checkbox" value="1" '.
					checked(1, $options['js'], false).'/>';
		echo '<label for="shortcode">Use Javascript</label>';
				
		// log
		echo '<br>$options[\'js\']='.$options['js'];
	}
	
	// visual/shortcode view
	function aug_db_sync_callback() {
		$options = get_option('aug_db_options');
		
		echo '<input id="sync_external" name="aug_visual_options[sync_external]" '.
					'type="checkbox" value="1" '.
					checked(1, $options['sync_external'], false).'/>';
		echo '<label for="sync_external">Enable Sync</label>';
				
		// log
		echo '<br>$options[\'sync_external\']='.$options['sync_external'];
	}
	
	// db/pass phrase view
	function aug_pass_phrase_callback() {
		$options = get_option('aug_db_options');
		
		echo 'Pass Phrase:';
		echo '<input type="text" id="pass_phrase"'.
				'name="aug_db_options[pass_phrase]"'.
				'value="'.$options['pass_phrase'].'" />';

		// TODO Generate btn 만들어서 임의 난수로 만들어주기
		// ...
		
		// log
		echo '<br>$options[\'pass_phrase\']='.$options['pass_phrase'];
	}
	
	public function create_plugin_settings_page() {
		// Add the menu item and page
		$page_title = 'My Awesome Settings Page';
		$menu_title = 'August';
		$capability = 'manage_options';
		$slug = 'august_settings';
		$callback = array( 'August_Setting_View', 'plugin_settings_page_content' );
		$icon = 'dashicons-admin-plugins';
		$position = 100;
		add_menu_page( $page_title, $menu_title, $capability, $slug, $callback, $icon, $position );
	}
	
	public function plugin_settings_page_content() {
		?>
		<div class="warp">
		
		<?php
			$active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'visual_options';  
        ?> 
			<h2 class="nav-tab-wrapper">  
            	<a href="?page=august_settings&tab=visual_options"
				   class="nav-tab <?php echo $active_tab == 'visual_options' ? 'nav-tab-active' : ''; ?>">
				   Visual
			    </a>  
            	<a href="?page=august_settings&tab=db_options"
				   class="nav-tab <?php echo $active_tab == 'db_options' ? 'nav-tab-active' : ''; ?>">
				   DB
			    </a>  
            	<a href="?page=august_settings&tab=seo_options"
				   class="nav-tab <?php echo $active_tab == 'seo_options' ? 'nav-tab-active' : ''; ?>">
				   SEO
			    </a>
        	</h2>
			
			<form method="post" action="options.php">
				
				<?php
            		if( $active_tab == 'visual_options' ) {
                		settings_fields( 'aug_visual_options' );
                		do_settings_sections( 'aug_visual_options' ); 
            		} else if( $active_tab == 'db_options' ) {
            			settings_fields( 'aug_db_options' );
            			do_settings_sections( 'aug_db_options' );
            		} else if ( $active_tab == 'seo_options') {
            			settings_fields( 'aug_seo_options' );
            			do_settings_sections( 'aug_seo_options' );
            		}
            	?>          

				<?php submit_button(); ?>				

			</form>
		</div><!-- /.wrap -->
		<?php
	}
	
}

?>