<?php
class RestProducts {
	function __construct() {
	}
	
	public function getAll() {
		$products = $this->get_products();
		$json = json_encode($products);
		return $json; // = response;
		// post 정보-> response[0];
		// 가격정보 등 쓸만한 정보 -> response[0].data.price
	}
	
	/*
	public function getOne(id) {
		// ...
	}
	*/
	
	
	/**
	 * 변경할수 있는 값:
	 * 	- data.name (제목)
	 * 	- data.price (가격) 'number string'
	 * 	- data.regular_price (정가격) 'number string'
	 * 	- data.sale_price (세일가) 'number string'
	 *  - data.status (발행상태) 'publish', 'draft'
	 *  - data.stock_quantity (number)
	 *  - data.stock_status ('instock', '')
	 *  - data.short_description
	 *  - data.description
	 * 
	 * @param number $id
	 * @param object $newProduct
	 */ 
	public function update($id, $newData) {
		$newName = $newData['name'];
		$newPrice = $newData['price'];
		
		// default fields
		$new_post = array();
		if ($newName != NULL) $new_post['post_title'] = $newName;
		
		if (0 < count($new_post)) {
			$new_post['ID'] = $id;
			wp_update_post($new_post);
		}
		
		// woocommerce meta
		// if ($newPrice != NULL) update_post_meta($id, 'price', "1");
		
		// return result
		// ...
		
		return true;
		
	}
	
	function get_products() {
		$args = array(
			'post_type' => 'product',
			'fields' => 'ids'
		);
		$productIds = query_posts($args);
		
		$_pf = new WC_Product_Factory();
		$products = array();
		foreach ($productIds as $id) {
			$product = $_pf->get_product($id);
			$products[] = $this->get_public_object($product);
		}
		return $products;
	}
	
	function get_public_object($object) {
		$public = [];
		$reflection = new ReflectionClass($object);
		foreach ($reflection->getProperties() as $property) {
			$property->setAccessible(true);
			$public[$property->getName()] = $property->getValue($object);
		}
		return json_decode(json_encode($public));
	}
}
?>