<?php
require dirname(__FILE__).'/products.php';
require dirname(__FILE__).'/orders.php';
require_once(dirname(__FILE__)."./../../../../wp-load.php");

$passPhrase = filter_input( INPUT_GET, 'pass_phrase', FILTER_SANITIZE_URL );
$options = get_option('aug_db_options');

// 추후, case 안에서도 요청 구분이 달라진다 (getAll, getOne, getByCondition...)
if ($passPhrase== $options['pass_phrase']) {
	$contentType = filter_input( INPUT_GET, 'content_type', FILTER_SANITIZE_URL );
	$method = $_SERVER['REQUEST_METHOD'];
	if ($method=== 'GET') {
		switch ($contentType) {
			
			case 'products':
				$restProducts = new RestProducts();
				echo $restProducts->getAll();
			break;
				
			case 'orders':
				$restOrders = new RestOrders();
				echo $restOrders->getAll();
			break;
			
		}
	}
	if ($method=== 'PUT' || $method=== 'POST') {
		$targetId = filter_input( INPUT_GET, 'id', FILTER_SANITIZE_URL );
		$entityBody = file_get_contents('php://input');
		$json =  json_decode($entityBody, TRUE);
		switch ($contentType) {
			
			case 'products':
				$restProducts = new RestProducts();
				echo $restProducts->update($targetId, $json);
			break;
				
			case 'orders':
				$restOrders = new RestOrders();
			break;
			
		}
	}
} else {
	echo 'fail: wrong pass phrase';
}

?>