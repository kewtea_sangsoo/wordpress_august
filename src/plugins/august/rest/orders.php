<?php
class RestOrders {
	function __construct(){
	}
	
	public function update($id, $newData) {
		$newCustomerNote = $newData['customer_note'];
		
		$newPaymentMethod = $newData['payment_method'];
		$newPaymentMethodTitle = $newData['payment_method_title'];
		
		// shipping object
		$newShippingAddress1 = $newData['address_1'];
		$newShippingAddress2 = $newData['address_2'];
		$newShippingCity = $newData['city'];
		$newShippingCompany = $newData['compnay'];
		$newShippingCountry = $newData['country'];
		$newShippingFirstname = $newData['first_name'];
		$newShippingLastname = $newData['last_name'];
		$newShippingPostcode = $newData['postcode'];
		$newShippingState = $newData['state'];
		
		$newStatus = $newData['status'];
		
		if ($newCustomerNote != NULL)
			update_post_meta($id, 'customer_note', $newCustomerNote);
		// ...
		
		// return result
		// ...
		
	}
	
	public function getAll() {
		$orders = $this->get_orders();
		$json = json_encode($orders, JSON_PRETTY_PRINT);
		return $json;
	}
	
	function get_orders() {
		$args = array(
			'post_type'   => wc_get_order_types(),
    		'post_status' => array_keys( wc_get_order_statuses() ),
			'fields'	  => 'ids'
		);
		$orderIds = query_posts($args);
		$orders = array();
		foreach($orderIds as $id) {
			$order = new WC_Order( $id );
			$orders[] = $this->get_public_object($order);
		}
		return $orders;
	}
	
	function get_public_object($object) {
		$public = [];
		$reflection = new ReflectionClass($object);
		foreach ($reflection->getProperties() as $property) {
			$property->setAccessible(true);
			$public[$property->getName()] = $property->getValue($object);
		}
		return json_decode(json_encode($public));
	}
}
?>