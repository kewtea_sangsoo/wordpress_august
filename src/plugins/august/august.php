<?php
/*
 * Plugin Name: August by kewtea
 * Plugin URI: https://kewtea.com/
 * Description: August Plugin
 * Version: 0.3
 * Author: Kewtea
 * Author URI: https://kewtea.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Requires at least: 4.4
 * Tested up to: 4.7
 */

require dirname(__FILE__).'/include/aug_adminview.php';
require dirname(__FILE__).'/module/shortcode/Shortcode_Handler.php';

class August {

	function __construct() {
		$this->init();
		$this->init_hooks();
	}

	function init_hooks() {
		register_activation_hook( __FILE__, 'august_activate' );
	}

	function init() {
		add_action( 'admin_menu',
				array( 'August_Setting_View', 'create_plugin_settings_page' ) );
		add_action( 'admin_init',
				array( 'August_Setting_View', 'setup_sections_fields' ) );
		
		// intercept action by options ...
		$visual_options = get_option('aug_visual_options');
		if ($visual_options['product_redirect']) {
			// UI (editor)
			add_action( 'woocommerce_product_options_general_product_data',
					array( $this, 'woo_add_custom_general_fields' ) );
			// value save (editor)
			add_action( 'woocommerce_process_product_meta',
					array( $this, 'woo_add_custom_general_fields_save' ) );
			
			// product views
			add_filter( 'woocommerce_loop_add_to_cart_link',
					array( $this, 'product_redirect_cart' ) );

			// product link chnage (intercept woocommerce action)
			add_action( 'template_redirect',
					array( $this, 'filter_change' ) );
		}
		if ($visual_options['product_reviews'] == 'gsheet') {
			add_action( 'woocommerce_after_single_product',
					array( $this, 'load_gsheet_reviews' ) );
		}
		if ($visual_options['product_reviews'] == 'hide') {
			add_filter( 'woocommerce_product_tabs',
					array( $this, 'remove_reviews' ), 98 );
		}
		if ($visual_options['promotion']) {
			add_action( 'template_redirect',
					array( $this, 'show_promotion' ) );
		}
		if ($visual_options['product_share']) {
			add_action( 'woocommerce_after_single_product',
					array( $this, 'load_share_btn' ) );
		}
		if ($visual_options['shortcode']) {
			$shortcode_handler = new Shortcode_Handler;
			add_shortcode( 'august', array( $shortcode_handler, 'build_shortcode' ) );
			wp_register_script(
				'jquery',
				'http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js',
				array(),
				null,
				false
			);
			/*
			wp_enqueue_script(
				'bootstrap-js',
				plugins_url( '/include/js/bootstrap.min.js', __FILE__ ),
				array( 'jquery' ),
				null,
				false
			);
			*/
		}
		
		// $db_options = get_option('aug_db_options');
		// ...
	}
	
	function load_share_btn() {
		global $product;
		
		// Register the library again from Google's CDN
		wp_register_script(
			'jquery',
			'http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js',
			array(),
			null,
			false
		);
		wp_enqueue_script(
			'share-js-qr',
			plugins_url( '/include/sharejs/js/jquery.qrcode.min.js', __FILE__ ),
			array( 'jquery' ),
			null,
			false
		);
		/*
		wp_enqueue_script(
			'bootstrap-js',
			plugins_url( '/include/js/bootstrap.min.js', __FILE__ ),
			array(),
			null,
			false
		);
		*/
		
		wp_enqueue_script(
			'share-js-line',
			plugins_url( '/include/sharejs/js/custom-line.js', __FILE__ ),
			array( 'jquery' ),
			null,
			false
		);
		wp_enqueue_script(
			'share-js-core',
			plugins_url( '/include/sharejs/js/jquery.share.kewtea.js', __FILE__ ),
			array( 'jquery' ),
			null,
			false
		);
		
		wp_enqueue_script(
			'share-ui',
			plugins_url( '/include/js/ShareUI.js', __FILE__ ),
			array( 'jquery' ),
			null,
			false
		);
		
		/*
		wp_enqueue_style(
			'bootstrap-modal-style',
			plugins_url( '/include/css/bootstrap.min.css', __FILE__ )
		);
		*/
		
		wp_enqueue_style(
			'share-js-style',
			plugins_url( '/include/sharejs/css/share.css', __FILE__ )
		);
		
		$title = $product->name;

		$images =
		wp_get_attachment_image_src(
			get_post_thumbnail_id( $product->id), 'single-post-thumbnail'
		);
		$mainImageURL = $images[0];
		?>
		<script>
			var title = "<?php echo $title ?>";
			var mainImageURL = "<?php echo $mainImageURL ?>";
		</script>		
		<?php 
		wp_enqueue_script(
			'share-product',
			plugins_url( '/include/js/share-product.js', __FILE__ ),
			array( 'jquery' ),
			null,
			false
		);
	}
	
	function show_promotion() {
		$options = get_option('aug_visual_options');
		
		// Register the library again from Google's CDN
		wp_register_script(
			'jquery',
			'http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js',
			array(),
			null,
			false
		);
		// gsheet
		wp_enqueue_script(
			'gsheet-service',
			plugins_url( '/include/js/gsheet.js', __FILE__ ),
			array( 'jquery' ),
			null,
			false
		);
		// PromotionBanner.js
		wp_enqueue_script(
			'promotion-banner',
			plugins_url( '/include/js/PromotionBanner.js', __FILE__ ),
			array( 'jquery' ),
			null,
			false
		);
		
		// promotion.css
		wp_enqueue_style(
			'promotion-banner-style',
			plugins_url( '/include/css/promotion.css', __FILE__ )
		);
		
		//get page's path
		$pagePath = parse_url( $_SERVER['REQUEST_URI'] );
		$pagePath = $pagePath['path'];
		?>
		<script>
			var sheetId = "<?php echo $options['promotion_gsheet_id'] ?>";
			var sheetNum = "<?php echo $options['promotion_gsheet_num'] ?>";
			var pagePath = "<?php echo $pagePath ?>";
		</script>		
		<?php
		// get external reviews
		wp_enqueue_script(
			'get-promotions',
			plugins_url( '/include/js/get-promotions.js', __FILE__ ),
			array( 'jquery','gsheet-service' ),
			null,
			true
		);
	}
	
	function remove_reviews($tabs) {
		unset($tabs['reviews']);
		return $tabs;
	}
	
	function load_gsheet_reviews() {
		global $product;
		$options = get_option('aug_visual_options');
		$productId = $product->id;

		// Register the library again from Google's CDN
		wp_register_script(
			'jquery',
			'http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js',
			array(),
			null,
			false
		);
		// gsheet
		wp_enqueue_script(
			'gsheet-service',
			plugins_url( '/include/js/gsheet.js', __FILE__ ),
			array( 'jquery' ),
			null,
			false
		);
		?>
		<script>
			var sheetId = "<?php echo $options['review_gsheet_id'] ?>";
			var sheetNum = "<?php echo $options['review_gsheet_num'] ?>";
			var productId = "<?php echo $productId ?>";
		</script>		
		<?php
		// get external reviews
		wp_enqueue_script(
			'get-external-reviews',
			plugins_url( '/include/js/get-external-reviews.js', __FILE__ ),
			array( 'jquery','gsheet-service' ),
			null,
			true
		);
	}
	
	function filter_change() {
		// cover
		remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
		add_action( 'woocommerce_before_shop_loop_item', array( $this, 'product_redirect' ), 10 );
		// detail
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
		add_action( 'woocommerce_single_product_summary', array( $this, 'single_add_cart'), 10 );
	}
	
	// product detail, add to cart button
	function single_add_cart() {
		global $product;
		$product_type = get_post_meta($product->id, 'product-type', true);
		$redirect_url = get_post_meta($product->id, 'redirect-url', true);
		if ($product_type == "redirect-url") {
			echo '<form class="cart" onsubmit="return false">'.
					 	'<button class="single_add_to_cart_button button alt">'.
					 		'<a href="'.$redirect_url.'" target="_blank">Redirect</a>'.
						'</button>'.
				 	 '</a>'.
				 '</form>';
		} else {
			do_action( 'woocommerce_' . $product->get_type() . '_add_to_cart' );
		}
	}
	
	
	// cover-product 전체를 감싸고있는 <a>
	function product_redirect() {
		global $product;
		$product_type = get_post_meta($product->id, 'product-type', true);
		$redirect_url = get_post_meta($product->id, 'redirect-url', true);
		if ($product_type == "full-redirect") {
			echo '<a target="_blank" href="' . $redirect_url. '" class="woocommerce-LoopProduct-link">';
		} else {
			echo '<a href="' . get_the_permalink() . '" class="woocommerce-LoopProduct-link">';
		}
	}
	
	// cover-product Add to cart 버튼
	function product_redirect_cart( $link ) {
		global $product;
		$product_type = get_post_meta($product->id, 'product-type', true);
		$redirect_url = get_post_meta($product->id, 'redirect-url', true);
		
		$add_to_cart_url = $product->add_to_cart_url();
		$target = '_self';
		$add_to_cart_text = $product->add_to_cart_text();
		if ($product_type == 'redirect-url') {
			$add_to_cart_url = $redirect_url;
			$target = '_blank';
			$add_to_cart_text = 'Redirect';
		}
			
		$link = sprintf( '<a rel="nofollow" href="%s" target="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
			esc_url( $add_to_cart_url),
			esc_attr( $target ),
			esc_attr( isset( $quantity ) ? $quantity : 1 ),
			esc_attr( $product->get_id() ),
			esc_attr( $product->get_sku() ),
			esc_attr( isset( $class ) ? $class : 'button' ),
			esc_html( $add_to_cart_text )
		);
		
		return $link;
	}
	
	// product editor dropdown, text-input
	function woo_add_custom_general_fields() {
		global $woocommerce, $post;
		?>
		<div class="options_group">
		<?php
		
		// Dropdown select (type of redirect)
		woocommerce_wp_select (
			array (
				'id'      => 'product-type',
				'label'   => __( 'Product Type', 'woocommerce' ),
				'options' => array(
								'in-house'   => __( 'In-house (default)', 'woocommerce' ),
								'redirect-url'   => __( 'Redirect to Buy button', 'woocommerce' ),
								'full-redirect' => __( 'Redirect (no detail page)', 'woocommerce' )
							)
			)
		);
		
		// Text Field (URL to redirect)
		woocommerce_wp_text_input(
			array(
				'id'          => 'redirect-url',
				'label'       => __( 'Redirect URL', 'woocommerce' ),
				'placeholder' => 'http://'
			)
		);
		
		?>
		</div><!-- /.options_group -->
		<script>
			jQuery(".product-type_field select").change(checkRedirectField);
			function checkRedirectField() {
				var type = jQuery(".product-type_field select").val();
				var state = "in-house" != type ? "block" : "none";
				jQuery(".redirect-url_field").css("display", state);
			}
			checkRedirectField();
		</script>
		<?php
	}
	
	// product editor save function
	function woo_add_custom_general_fields_save($post_id) {
		$woocommerce_select = $_POST['product-type'];
		if (!empty( $woocommerce_select )) {
			update_post_meta( $post_id, 'product-type', esc_attr( $woocommerce_select ) );
			if ( 'in-house' != esc_attr( $woocommerce_select ) ) {
				$woocommerce_text_field = $_POST['redirect-url'];
				if (!empty( $woocommerce_text_field )) {
					update_post_meta(
						$post_id,
						'redirect-url',
						esc_attr( $woocommerce_text_field )
					);
				}
			} else {
				update_post_meta( $post_id, 'redirect-url', '' );
			}
		}
	}
	
}

global $august;
$august = new August();

?>