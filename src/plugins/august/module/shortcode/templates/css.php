<?php 
	$no_padding_container = $data['no_padding_container'];
	$no_padding_banner = $data['no_padding_banner'];
	$body_color = $data['body_color'];
	$banner_margin = $data['banner_margin'];
	$slide_margin = $data['$slide_margin'];
?>

<style>

<?php if ($no_padding_container): ?>
	div#container-inner { width: initial; margin: 0; padding: 0; }
<?php endif ?>

<?php if ($no_padding_banner): ?>
	.kt-row { margin-left: 0; margin-right: 0; }
	.kt-row > * { padding-left: 0; padding-right: 0; }
<?php endif ?>
	
<?php if ($body_color): ?>
	div#container-inner { background-color: <?php echo $body_color; ?> }
<?php endif ?>

<?php if ($banner_margin): ?>
	.kt-banner { margin: <?php echo $banner_margin; ?> 0; }
<?php endif ?>

<?php if ($slide_margin): ?>
	.kt-slider { padding: <?php echo $slide_margin; ?> 0; }
<?php endif ?>
	
</style>