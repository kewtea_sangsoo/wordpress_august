<?php 
	$type = $options['type'];
	$image = $data['image'];
	$link = $data['link'];
	$desc = $data['desc']; 			// type == 'bio'
	$title = $data['title'];		// type == 'image'
	$subtitle = $data['subtitle'];	// type == 'image'
?>

<a class="kt-banner kt-banner-type-<?php echo $type; ?>
	col-xxs-<?php echo $xxs_num; ?>
	col-xs-<?php echo $xs_num; ?>
	col-sm-<?php echo $sm_num; ?>
	col-md-<?php echo $md_num; ?>
	col-lg-<?php echo $lg_num; ?>"
	style="min-height: <?php echo $height; ?>; height: 100%"
	<?php if ($link): ?>
	href="<?php echo $link; ?>"
	<?php endif ?>
>

	<?php if ($type == 'bio'): ?>
		<div class="row kt-row">
			<div class="kt-banner-image-wrapper col-xxs-6 col-xs-6 col-sm-4 col-md-4 col-lg-4"
				style="min-height: <?php echo $height; ?>;">
				<div class="kt-banner-image" style="background-image: url('<?php echo $image; ?>');"></div> 
			</div>
			<div class="kt-banner-desc col-xxs-6 col-xs-6 col-sm-8 col-md-8 col-lg-8">
				<?php echo $desc; ?>
			</div>
		</div>	
	<?php endif ?>
	
	<?php if ($type == 'image'): ?>
		<div class="kt-banner-image" style="background-image: url('<?php echo $image; ?>'); height: <?php echo $height; ?>;">
			<div class="kt-banner-title-wrapper">
				<div class="kt-banner-title">
					<?php echo $title; ?>
					<?php if ($subtitle): ?>
						<span class="separator animate" style="background-color: #ffffff;height: 1px;"></span>
						<div class="kt-banner-subtitle"><?php echo $subtitle; ?></div>			
					<?php endif ?>
				</div>
			</div>	
		</div>
	<?php endif ?>

</a>