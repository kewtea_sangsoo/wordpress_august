<?php
	$temp_id = rand(0,1000).rand(0,1000);
	$type = $options['type'] || 'horizontal';
	$interval = $options['interval'];
?>

<div id="carousel-example-generic-<?php echo $temp_id; ?>" class="kt-slider carousel <?php echo $type; ?> slide" data-ride="carousel"
	data-interval="<?php echo $interval?>" style="height: <?php echo $height; ?>">
	
	<div class="carousel-inner" role="listbox">
	<?php
	for ($i=0; $i<sizeof($data); $i++) {
		$content = $data[$i];
		$title = $content['title'];
		$link = $content['link'];
		$image = $content['image'];
	?>
		<div class="item <?php if($i == 0):?> active <?php endif?>">
			<?php if($link): ?>
			<a href="<?php echo $link; ?>">
			<?php endif?>
			
				<div class="slide-image" style="background-image: url('<?php echo $image; ?>')"></div>
				
				<?php if($title): ?>
				<div class="slide-title"><?php echo $title; ?></div>
				<?php endif?>
			
			<?php if($link): ?>
			</a>
			<?php endif?>
		</div>
	<?php
	}
	?>
	</div>
	
</div>