<?php 
	$title = $data['title'];
	$subtitle = $data['subtitle'];
	$webm = $data['webm'];
	$mp4 = $data['mp4'];
	$ogg = $data['ogg'];
	$image = $data['image'];
	$link = $data['link'];
?>
<a class="kt-banner kt-banner-type-video col-xxs-12 col-xs-12 col-sm-12 col-md-12 com-lg-12" style="height: 100%"<?php if ($link): ?> href="<?php echo $link; ?>"<?php endif ?>>
	<video class="kt-banner-video" width="100%" preload="auto" loop="" autoplay="" muted="" poster="<?php echo $image; ?>">
		<?php if ($webm): ?><source type="video/webm" src="<?php echo $webm; ?>"><?php endif ?>
		<?php if ($mp4): ?><source type="video/mp4" src="<?php echo $mp4; ?>"><?php endif ?>
		<?php if ($ogg): ?><source type="video/ogg" src="<?php echo $ogg; ?>"><?php endif ?>
	</video>
	<div class="kt-banner-image" style="background-image: url('<?php echo $image; ?>')"></div>
	<?php if ($title || $subtitle): ?>
	<div class="kt-banner-title-wrapper">
		<div class="kt-banner-title"><?php echo $title; ?></div>
		<?php if ($subtitle): ?>
		<div class="kt-banner-subtitle"><?php echo $subtitle; ?></div>
		<?php endif ?>
	</div>
	<?php endif ?>
</a>