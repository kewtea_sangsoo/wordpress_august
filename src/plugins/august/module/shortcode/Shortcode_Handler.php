<?php
require 'template.php'; // class Template

class Shortcode_Handler {
	
	function __construct() { }
	
	function get_attr_by_atts($atts) {
		return shortcode_atts( array(
			'style'		=> '',	// unibox, slide, ...
			'width'		=> '100%',
			'height'	=> '250px',
			'xxs-col'	=> '6',
			'xs-col'	=> '6',
			'sm-col'	=> '6',
			'md-col'	=> '6',
			'lg-col'	=> '6',
			'full-col'	=> false,		
			'data'		=> '', // JSON String
			// optional (style 에 따라 쓰이기도하고, 안쓰이기도 하는 것들)
			'type'		=> '',
			'interval'	=> '3000'
		), $atts );
	}
	
	// TODO 상수로 선언? (http://php.net/manual/kr/language.oop5.constants.php)
	function get_template_by_style($style) {
		switch ($style) {
			case 'unibox': return 'templates/unibox.php';
			case 'video': return 'templates/video.php';
			case 'slide': return 'templates/slide.php';
			case 'css': return 'templates/css.php';
		}
	}
	//
	
	function get_data($attr) {
		$data_string = $attr['data'];
		if ($attr['style'] == 'slide') $data_string = '['.$data_string.']';
		return json_decode($data_string, true);
	}
	
	public function build_shortcode($atts) {
		// get attribute
		$attr = $this->get_attr_by_atts($atts);
		
		// build view
		$view = new Template();
		$view->width = $attr['width'];
		$view->height = $attr['height'];
		
		$view->xxs_num = $attr['xxs-col'];
		$view->xs_num = $attr['xs-col'];
		$view->sm_num = $attr['sm-col'];
		$view->md_num = $attr['md-col'];
		$view->lg_num = $attr['lg-col'];
		
		if ($attr['full-col']) {
			$view->xxs_num = 12;
			$view->xs_num = 12;
			$view->sm_num = 12;
			$view->md_num = 12;
			$view->lg_num = 12;
		}
		
		$view->data = $this->get_data($attr);
		$view->options = $attr;
		
		return $view->render($this->get_template_by_style($attr['style']));
	}
	
}
?>